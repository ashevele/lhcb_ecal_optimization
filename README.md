LHCb ECAL optimization
======================

The project dedicated to LHCb ECAL Upgrade Ib/II studies. The project based on [LHCb Gauss](https://gitlab.cern.ch/lhcb/Gauss/) and [Delphes](https://github.com/delphes/delphes) simulation frameworks and depends on the LHCb environment.

Branches
--------
The following branches are currently active:

Branch             | Purpose                          | Main Project, Release Series
-------------------|--------------------------------- | ---------------------------------------
[master](https://gitlab.cern.ch/lambda-hse/lhcb_ecal_optimization/tree/master/)             | ECAL Upgrade development         |
[initial](https://gitlab.cern.ch/lambda-hse/lhcb_ecal_optimization/tree/initial/)            | Minimal working example          |
[dev](https://gitlab.cern.ch/lambda-hse/lhcb_ecal_optimization/tree/dev/)                | Bleeding edge development        |


Running the code using Docker
---------------------------------------
```
docker pull qwaiqir/calosim_build:0.2
docker run -h dev --privileged -it -p 8968:8968 qwaiqir/calosim_build:0.2 bash
su - nb_user
. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p4/InstallArea/scripts/LbLogin.sh -c x86_64-centos7-gcc62-opt
cd Gauss/Sim/LbDelphes/options
../../../build.x86_64-centos7-gcc62-opt/run gaudirun.py Gauss-Job.py
jupyter notebook --ip 0.0.0.0 --port 8968 --no-browser --allow-root
```


How to build and run the code on LXPLUS
---------------------------------------
(tested on lxplus7 with a LHCb environment)

```
. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p4/InstallArea/scripts/LbLogin.sh -c x86_64-centos7-gcc62-opt
git clone https://gitlab.cern.ch/lambda-hse/lhcb_ecal_optimization.git
cd lhcb_ecal_optimization
cd Gauss
lb-project-init
make configure
make -j8 LbDelphes
make -j8
```

Run from Gauss/Sim/LbDelphes/options:

```
../../../build.x86_64-centos7-gcc62-opt/run gaudirun.py Gauss-Job.py
```

How to build the code using Docker
---------------------------------------

```
git clone https://gitlab.cern.ch/lambda-hse/lhcb_ecal_optimization.git
docker build -t "calosim:0.1" .
docker run -h dev --privileged -it calosim:0.2 bash
. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p4/InstallArea/scripts/LbLogin.sh -c x86_64-centos7-gcc62-opt
cd Gauss
lb-project-init
make configure
make -j8 LbDelphes
make -j8
```

You can separate the environment variables inside the Docker container by switching to passwordless user:
```
su - nb_user
```

Restore the environment variables (in case you have already used the LbLogin script):
```
. ./local_ROOT_python.sh
```

Tips
----

- The geometry of ECAL described in the [delphes card](https://gitlab.cern.ch/lambda-hse/lhcb_ecal_optimization/tree/master/Gauss/Sim/LbDelphes/options/cards/delphes_card_LHCb_EndVelo_Rich2_withEcal.tcl#L333)
- Possible input decay files are listed [here](http://lhcbdoc.web.cern.ch/lhcbdoc/decfiles/releases/latest/table_evttype.php)
- Feel free to look on and develop the [notebooks](https://gitlab.cern.ch/lambda-hse/lhcb_ecal_optimization/tree/master/notebooks)

Links
-----

- [Upgrade Ib/II calorimeter meeting](https://indico.cern.ch/event/762088/) (8 Oct 2018)


Acknowledgements
----------------

Initial code based on [code](https://gitlab.cern.ch/lhcb/Gauss/tree/wip-adavis2) written by Adam Davis.
