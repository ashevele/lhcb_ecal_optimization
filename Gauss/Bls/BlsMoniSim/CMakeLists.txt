################################################################################
# Package: BlsMoniSim
################################################################################
gaudi_subdir(BlsMoniSim v2r0p1)

gaudi_depends_on_subdirs(Event/MCEvent
                         GaudiAlg)

find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_module(BlsMoniSim
                 src/*.cpp
                 LINK_LIBRARIES MCEvent GaudiAlgLib)

