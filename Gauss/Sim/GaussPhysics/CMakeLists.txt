################################################################################
# Package: GaussPhysics
################################################################################
gaudi_subdir(GaussPhysics v11r1p1)

gaudi_depends_on_subdirs(Sim/GaussTools)

FindG4libs(LHCblists physicslists)

find_package(Boost)
find_package(CLHEP)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS} ${Geant4_INCLUDE_DIRS})

gaudi_add_module(GaussPhysics
                 src/*.cpp
                 LINK_LIBRARIES GaussToolsLib
                                ${GEANT4_LIBS})

