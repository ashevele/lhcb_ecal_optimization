// Include files 

// from Event/Event
#include "Event/MCParticle.h"
#include "Event/MCVertex.h"

#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "ConversionFilter.h"

//-----------------------------------------------------------------------------
// Implementation file for class : ConversionFilter
//
// 2017-10-26 : Michel De Cian
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( ConversionFilter )

ConversionFilter::ConversionFilter(const std::string& name, ISvcLocator* pSvcLocator)
: GaudiAlgorithm(name, pSvcLocator){

  declareProperty( "Mother"           , m_mother = ""                );
  declareProperty( "MaxSearchDepth"   , m_maxSearchDepth = 20        );
  declareProperty( "MatchSearchDepth" , m_matchSearchDepth = false   );
  declareProperty( "MaxZ"             , m_maxZ   = 500.0             );
  declareProperty( "MinP"             , m_minP   = 1500.0            );
  declareProperty( "MinPT"            , m_minPT  = 50.0              );
  declareProperty( "MinTheta"         , m_minTheta  = 0.010          );
  declareProperty( "MaxTheta"         , m_maxTheta  = 0.400          );
  
}
//=============================================================================
// Initialisation. Check parameters
//=============================================================================
StatusCode ConversionFilter::initialize() {

  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  if( m_mother == "" ){
    m_motherID = -1;
    m_maxSearchDepth = 1;
    return StatusCode::SUCCESS;
  }

  LHCb::IParticlePropertySvc* ppSvc = svc< LHCb::IParticlePropertySvc >( "LHCb::ParticlePropertySvc" , true ) ;
  if( !ppSvc ) {
    error() << "Could not retrieve ParticlePropertySvc" << endmsg;
    m_motherID = -1;
    m_maxSearchDepth = 1;
    return StatusCode::SUCCESS;
  }
  
  const LHCb::ParticleProperty* prop = ppSvc->find( m_mother );
  if( !prop ) {
    error() << "Could not retrieve ParticleProperty" << endmsg;
    m_motherID = -1;
    m_maxSearchDepth = 1;
    return StatusCode::SUCCESS;
  }

  // -- Get the ID of the mother (particle and antiparticle)
  m_motherID = prop->pdgID().abspid();
  
  info() << "Filtering on gamma conversion with " << m_mother << " in the decay chain" << endmsg; 

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode ConversionFilter::execute() {

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "==> Execute" << endmsg;

  LHCb::MCParticles* mcParts =
    getIfExists<LHCb::MCParticles>( LHCb::MCParticleLocation::Default );
  
  const std::string filterString = "event with gamma conversion from "+m_mother;

  bool filter = false;
  int nConv = 0;
  // -- Loop over all MCParticles
  // -- First check that they are gammas
  // -- do pair production at their endvertex and have the right z-position.
  for( auto mcPart : *mcParts){
    if( mcPart->particleID().pid() != 22 ) continue;
    bool goodPhoton = false;
    for( const auto endVtx : mcPart->endVertices()){
      if( !endVtx ) continue;
      if( endVtx->type() == LHCb::MCVertex::PairProduction && std::abs(endVtx->position().z()) < m_maxZ ){
        goodPhoton = true;
        break;
      }
    }
    if( !goodPhoton ) continue;
    nConv++;

    unsigned int searchDepth = 0;
    const LHCb::MCParticle* momPart = mcPart->mother();
    
    // -- Search up the decay tree to see if this gamma is coming from the mother we care for
    // -- If we don't care about the mother, we just check if it fullfills the p,pt and theta requirements
    while( momPart && searchDepth < m_maxSearchDepth){

      // -- If we only want to search for a given depth, and not for a maximum
      // -- loop until we have reach that depth.
      if( m_matchSearchDepth && searchDepth < m_maxSearchDepth - 1 ){
        searchDepth++;
        momPart = momPart->mother();
        continue;
      }
      

      if( (std::abs(momPart->particleID().pid()) == m_motherID) || (m_motherID == -1) ){
        if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "Found gamma conversion from mother: " << m_motherID << endmsg;
        // -- we found the right mother
        // -- let's see if the daughters (=electrons) have enough momentum, pt and are within the acceptance.
        for( const LHCb::MCVertex* endVtx : mcPart->endVertices()){
          if( !endVtx ) continue;
          if( endVtx->products().size() != 2 ) continue; // want pair production
          
          // -- We ask that all of the daughters have enough p and pt, and are within the LHCb acceptance. In case there is more than 1 end vertex
          // -- we take the "or" of them.
          filter |= std::all_of( std::begin(endVtx->products()), std::end(endVtx->products()), [&](const LHCb::MCParticle* dau){
              return (dau->p() > m_minP) && (dau->pt() > m_minPT) && (dau->momentum().theta() > m_minTheta) && (dau->momentum().theta() < m_maxTheta);
            });
          
          // --
          if( UNLIKELY( msgLevel(MSG::DEBUG) ) ){
            for( const LHCb::MCParticle* dau : endVtx->products()){
              debug() << "particle: " << dau->particleID().pid() << " p: " << dau->p() << " pt: " 
                      << dau->pt() << " theta: " << dau->momentum().theta() << endmsg;
            }
            if( filter ){
              debug() << "accepted" << endmsg;
            }else{
              debug() << "rejected" << endmsg;
            }
            
          }
          // --
        }
        // -- We have found the right mother, no need to loop deeper
        break;
      }
      searchDepth++;
      momPart = momPart->mother();
    }
  }

  if( UNLIKELY( msgLevel(MSG::DEBUG) ) ) debug() << "Found " <<  nConv << " gamma conversions in total" << endmsg;

  if( filter ) counter(filterString)++;
  
  setFilterPassed( filter );
  return StatusCode::SUCCESS;
  
}

