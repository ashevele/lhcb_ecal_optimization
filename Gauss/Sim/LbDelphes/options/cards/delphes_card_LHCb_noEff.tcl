#set MaxEvents 1000
#set RandomSeed 123


#######################################
# Order of execution of various modules
#######################################

set ExecutionPath {

  ParticlePropagator
  MuonsMomentumSmearing
  ChargedHadronsMomentumSmearing
  ElectronsMomentumSmearing    
  TrackMerger

}



#################################
# Propagate particles in cylinder
#################################

module ParticlePropagator ParticlePropagator {
  set InputArray Delphes/stableParticles
 
  set OutputArray stableParticles
  set ChargedHadronOutputArray chargedHadrons
  set ElectronOutputArray electrons
  set MuonOutputArray muons

  set OutputUpstreamArray upstreamParticles
  set ChargedHadronOutputUpstreamArray upstreamchargedHadrons
  set ElectronOutputUpstreamArray upstreamelectrons
  set MuonOutputUpstreamArray upstreammuons

  set OutputDownstreamArray downstreamParticles
  set ChargedHadronOutputDownstreamArray downstreamchargedHadrons
  set ElectronOutputDownstreamArray downstreamelectrons
  set MuonOutputDownstreamArray downstreammuons

  # radius of the magnetic field coverage, in m

  set Radius 3.31


  # half-length of the magnetic field coverage, in m
  set HalfLength 12.0

  # magnetic field
  set Bz 1.1

  # Need to veto anything with theta > 0.269 rad  -> eta = 2
  #                            theta < 0.0135 rad -> eta = 5

  # tracker and calos are at approx 0.269 rad, R = 12*tan(0.269)

}

########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto ChargedHadronsMomentumSmearing {
    set InputArray ParticlePropagator/chargedHadrons
    set OutputArray chargedHadrons
    
    # set ResolutionFormula {resolution formula as a function of eta and pt}
    
    # resolution formula for charged hadrons
    
    set ResolutionHisto ResSparse.root
}

#################################
# Energy resolution for electrons
#################################

module MomentumSmearingHisto ElectronsMomentumSmearing {
  set InputArray ParticlePropagator/electrons
  set OutputArray electrons

  # set ResolutionFormula {resolution formula as a function of eta and energy}
  set ResolutionHisto ResSparse.root

  }

###############################
# Momentum resolution for muons
###############################

module MomentumSmearingHisto MuonsMomentumSmearing {
  set InputArray ParticlePropagator/muons
  set OutputArray muons

  # resolution formula for muons
  set ResolutionHisto ResSparse.root
}



###############
## Track merger
###############

module Merger TrackMerger {
# add InputArray InputArray
#    add InputArray ChargedHadronEfficiency/chargedHadrons
#    add InputArray ElectronEfficiency/electrons
#    add InputArray MuonEfficiency/muons

    add InputArray ElectronsMomentumSmearing/electrons
    add InputArray MuonsMomentumSmearing/muons
    add InputArray ChargedHadronsMomentumSmearing/chargedHadrons

#    add InputArray ParticlePropagator/chargedHadrons
#    add InputArray ParticlePropagator/electrons
#    add InputArray ParticlePropagator/muons
    
# add InputArray ElectronEnergySmearing/electrons
# add InputArray MuonMomentumSmearing/muons
  set OutputArray tracks
}


