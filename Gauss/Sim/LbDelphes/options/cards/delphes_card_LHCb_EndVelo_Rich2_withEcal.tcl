#set MaxEvents 1000
#set RandomSeed 123


#######################################
# Order of execution of various modules
#######################################

set ExecutionPath {

  ParticlePropagatorEndVelo

  ChargedHadronMomentumSmearingEndVelo
  MuonsMomentumSmearingEndVelo
  ElectronsMomentumSmearingEndVelo

  ChargedHadronEfficiencyEndVelo
  ElectronEfficiencyEndVelo
  MuonEfficiencyEndVelo

  TrackMergerEndVelo
  ParticlePropagatorEndRich2

  ChargedHadronMomentumSmearingEndRich2
  MuonsMomentumSmearingEndRich2
  ElectronsMomentumSmearingEndRich2

  ChargedHadronEfficiencyEndRich2
  ElectronEfficiencyEndRich2
  MuonEfficiencyEndRich2


  TrackMergerEndRich2

  PhotonEnergySmearing
  Calorimeter 
  PhotonEfficiency

  FinalTrackMerger
}



#################################
# Propagate particles in cylinder
#################################

module ParticlePropagatorLHCb ParticlePropagatorEndVelo {
  set InputArray Delphes/stableParticles
 
  set OutputArray stableParticles
  set ChargedHadronOutputArray chargedHadrons
  set ElectronOutputArray electrons
  set MuonOutputArray muons
  set NeutralOutputArray neutrals
  set PhotonOutputArray photons

  set OutputUpstreamArray upstreamParticles
  set ChargedHadronOutputUpstreamArray upstreamchargedHadrons
  set ElectronOutputUpstreamArray upstreamelectrons
  set MuonOutputUpstreamArray upstreammuons

  set OutputDownstreamArray downstreamParticles
  set ChargedHadronOutputDownstreamArray downstreamchargedHadrons
  set ElectronOutputDownstreamArray downstreamelectrons
  set MuonOutputDownstreamArray downstreammuons


  # magnetic field
  set Bz 1
  set zEnd 770
  set FormulaZMagCenter {5022.74+1160*x + 6.36*10**8 *x**2 }
}

########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto ElectronsMomentumSmearingEndVelo {
    
    set InputArray ParticlePropagatorEndVelo/electrons
    set OutputArray electrons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHistoFile \$LBDELPHESROOT/options/ResSparse.root
    set ResolutionHisto sparseResEndVelo
    
}

########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto MuonsMomentumSmearingEndVelo {
    
    set InputArray ParticlePropagatorEndVelo/muons
    set OutputArray muons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHistoFile \$LBDELPHESROOT/options/ResSparse.root
    set ResolutionHisto sparseResEndVelo

}


########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto ChargedHadronMomentumSmearingEndVelo {
    
    set InputArray ParticlePropagatorEndVelo/chargedHadrons
    set OutputArray chargedHadrons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHistoFile \$LBDELPHESROOT/options/ResSparse.root
    set ResolutionHisto sparseResEndVelo

}


########################################                                                      

# Efficiency for charged hadrons

########################################
module EfficiencyHisto ChargedHadronEfficiencyEndVelo {
    set InputArray ChargedHadronMomentumSmearingEndVelo/chargedHadrons
#    set InputArray ParticlePropagator/chargedHadrons
    set OutputArray chargedHadrons
    set EfficiencyHisto \$LBDELPHESROOT/options/ResSparse.root
    set NumeratorHisto hReconstructible_EndVelo
    set DenominatorHisto hTxTyP_Delphes_EndVelo
}

########################################

# Efficiency for electrons

########################################

module EfficiencyHisto ElectronEfficiencyEndVelo {
#    set InputArray ParticlePropagator/electrons
    set InputArray ElectronsMomentumSmearingEndVelo/electrons
    set OutputArray electrons
    set EfficiencyHisto \$LBDELPHESROOT/options/ResSparse.root
    set NumeratorHisto hReconstructible_EndVelo
    set DenominatorHisto hTxTyP_Delphes_EndVelo
}

########################################

# Efficiency for muons

########################################

module EfficiencyHisto MuonEfficiencyEndVelo {
#    set InputArray ParticlePropagator/muons
    set InputArray MuonsMomentumSmearingEndVelo/muons
    set OutputArray muons
    set EfficiencyHisto \$LBDELPHESROOT/options/ResSparse.root
    set NumeratorHisto hReconstructible_EndVelo
    set DenominatorHisto hTxTyP_Delphes_EndVelo

}


###############
## Track merger
###############

module Merger TrackMergerEndVelo {
    add InputArray ChargedHadronEfficiencyEndVelo/chargedHadrons
    add InputArray ElectronEfficiencyEndVelo/electrons
    add InputArray MuonEfficiencyEndVelo/muons
    add InputArray ParticlePropagatorEndVelo/photons
    add InputArray ParticlePropagatorEndVelo/neutrals
#    add InputArray ParticlePropagatorEndVelo/chargedHadrons
#    add InputArray ParticlePropagatorEndVelo/electrons
#    add InputArray ParticlePropagatorEndVelo/muons
    set OutputArray tracks
}

#################################
# Propagate particles in cylinder
#################################

module ParticlePropagatorLHCb ParticlePropagatorEndRich2 {
  set InputArray TrackMergerEndVelo/tracks
 
  set OutputArray stableParticles
  set ChargedHadronOutputArray chargedHadrons
  set ElectronOutputArray electrons
  set MuonOutputArray muons

  set OutputUpstreamArray upstreamParticles
  set ChargedHadronOutputUpstreamArray upstreamchargedHadrons
  set ElectronOutputUpstreamArray upstreamelectrons
  set MuonOutputUpstreamArray upstreammuons

  set OutputDownstreamArray downstreamParticles
  set ChargedHadronOutputDownstreamArray downstreamchargedHadrons
  set ElectronOutputDownstreamArray downstreamelectrons
  set MuonOutputDownstreamArray downstreammuons
    

  # magnetic field
  set Bz 1
  set zEnd 12000
  set FormulaZMagCenter {5022.74+1160*x + 6.36*10**8 *x**2 }
}


########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto ElectronsMomentumSmearingEndRich2 {
    
    set InputArray ParticlePropagatorEndRich2/electrons
    set OutputArray electrons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHistoFile \$LBDELPHESROOT/options/ResSparse.root
    set ResolutionHisto sparseResEndRich2

}

########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto MuonsMomentumSmearingEndRich2 {
    
    set InputArray ParticlePropagatorEndRich2/muons
    set OutputArray muons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHistoFile \$LBDELPHESROOT/options/ResSparse.root
    set ResolutionHisto sparseResEndRich2
}


########################################
# Momentum resolution for charged tracks
########################################

module MomentumSmearingHisto ChargedHadronMomentumSmearingEndRich2 {
    
    set InputArray ParticlePropagatorEndRich2/chargedHadrons
    set OutputArray chargedHadrons
    # set ResolutionFormula {resolution formula as a function of eta and pt}    
    # resolution formula for charged hadrons    
    set ResolutionHistoFile \$LBDELPHESROOT/options/ResSparse.root
    set ResolutionHisto sparseResEndRich2

}


########################################                                                      

# Efficiency for charged hadrons

########################################
module EfficiencyHisto ChargedHadronEfficiencyEndRich2 {
    set InputArray ChargedHadronMomentumSmearingEndRich2/chargedHadrons
#    set InputArray ParticlePropagator/chargedHadrons
    set OutputArray chargedHadrons
    set EfficiencyHisto \$LBDELPHESROOT/options/ResSparse.root
    set NumeratorHisto hReconstructed_EndRich2
    set DenominatorHisto hReconstructible_EndRich2
}

########################################

# Efficiency for electrons

########################################

module EfficiencyHisto ElectronEfficiencyEndRich2 {
#    set InputArray ParticlePropagator/electrons
    set InputArray ElectronsMomentumSmearingEndRich2/electrons
    set OutputArray electrons
    set EfficiencyHisto \$LBDELPHESROOT/options/ResSparse.root
    set NumeratorHisto hReconstructed_EndRich2
    set DenominatorHisto hReconstructible_EndRich2
}

########################################

# Efficiency for muons

########################################

module EfficiencyHisto MuonEfficiencyEndRich2 {
#    set InputArray ParticlePropagator/muons
    set InputArray MuonsMomentumSmearingEndRich2/muons
    set OutputArray muons
    set EfficiencyHisto \$LBDELPHESROOT/options/ResSparse.root
    set NumeratorHisto hReconstructed_EndRich2
    set DenominatorHisto hReconstructible_EndRich2

}
###############
## Track merger
###############

module Merger TrackMergerEndRich2 {
    add InputArray ChargedHadronEfficiencyEndRich2/chargedHadrons
    add InputArray ElectronEfficiencyEndRich2/electrons
    add InputArray MuonEfficiencyEndRich2/muons

#    add InputArray ParticlePropagatorEndRich2/chargedHadrons
#    add InputArray ParticlePropagatorEndRich2/electrons
#    add InputArray ParticlePropagatorEndRich2/muons
    set OutputArray tracks
}

#########################################################
# now that everything is at the ecal, make calo response
#########################################################

module EnergySmearing PhotonEnergySmearing {  
   set InputArray ParticlePropagatorEndRich2/stableParticles
   set OutputArray SmearParticles

    set ResolutionFormula {
	sqrt(energy^2*0.107^2 + energy*2.08^2)
    }

}

module CalorimeterLHCb Calorimeter {
#   set ParticleInputArray PhotonEnergySmearing/SmearParticles 
   set ParticleInputArray ParticlePropagatorEndRich2/stableParticles
   set TrackInputArray TrackMergerEndRich2/tracks

   set TowerOutputArray ecalTowers
   set PhotonOutputArray photonArray
   
   set EFlowTrackOutputArray eflowTracks
   set EFlowTowerOutputArray eflowTower
   set EFlowPhotonOutputArray eflowPhoton
   set EFlowNeutralHadronOutputArray eflowNeutralHadrons
   set CaloBoarders caloBoarders
   set SmearTowerCenter false
#   set IsEcal true

   set ZECal 12000.

   set ECalEnergyMin 0
   set HCalEnergyMin 0
 
   set ECalEnergySignificanceMin 1.0
   set HCalEnergySignificanceMin 1.0

   set SmearTowerCenter true

   set pi [expr {acos(-1)}]


   set BorderX [list 323.2 969.6 1939.2 3878.4]
   set BorderY [list 323.2 727.2 1212.0 3151.2]
 
   set YBins {}
   for {set i 0} {$i <= 36} {incr i} {
       add YBins [expr { -727.2 + $i * 40.4 }]
   } 
   for {set i 0} {$i <= 48} {incr i} {
       set X [expr { -969.6 + $i * 40.4 }]
       add XYBins1 $X $YBins
   }

   set YBins {}
   for {set i 0} {$i <= 40} {incr i} {
       add YBins [expr {-1212.0 + $i * 60.6 }]
   } 
   for {set i 0} {$i <= 64} {incr i} {
       set X [expr {-1939.2 + $i * 60.6 }]
       add XYBins2 $X $YBins
   }

   set YBins {}
   for {set i 0} {$i <= 52} {incr i} {
       add YBins [expr {-3151.2 + $i * 121.2 }]
   } 
   for {set i 0} {$i <= 64} {incr i} {
       set X [expr {-3878.4 + $i * 121.2 }]
       add XYBins3 $X $YBins
   }



   add EnergyFraction {0} {0.0 1.0}
   add EnergyFraction {11} {1.0 0.0}
   add EnergyFraction {22} {1.0 0.0}
   #add EnergyFraction {111} {1.0 0.0}
   #add EnergyFraction {211} {0.0 0.0}
   #add EnergyFraction {321} {0.0 0.0}
   add EnergyFraction {12} {0.0 0.0}
   add EnergyFraction {13} {0.0 0.0}
   add EnergyFraction {14} {0.0 0.0}
   add EnergyFraction {16} {0.0 0.0}
   #add EnergyFraction {2212} {0.0 0.0}

   add EnergyFraction {1000022} {0.0 0.0}
   add EnergyFraction {1000023} {0.0 0.0}
   add EnergyFraction {1000025} {0.0 0.0}
   add EnergyFraction {1000035} {0.0 0.0}
   add EnergyFraction {1000045} {0.0 0.0}


   add EnergyFraction {310} {0.3 0.7}
   add EnergyFraction {3122} {0.3 0.7}
   
   set ECalResolutionFormula {(eta<=6.0 && eta > 1.0) * sqrt(energy^2*0.01^2 + 1000*energy*0.10^2 )}
   set HCalResolutionFormula {(eta<=6.0 && eta > 1.0) * sqrt(energy^2*0.01^2 + 1000*energy*0.10^2 )}


}




module Efficiency PhotonEfficiency {
   set InputArray Calorimeter/eflowPhoton 
   set OutputArray EffPhotons
}

module Merger FinalTrackMerger {
    add InputArray TrackMergerEndRich2/tracks
    add InputArray Calorimeter/eflowPhoton
    #add InputArray Calorimeter/eflowNeutralHadrons
    set OutputArray tracks
}

