// Include files 

// from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IDataManagerSvc.h"



// Event.
#include "Event/HepMCEvent.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"

// HepMC.
#include "GenEvent/HepMCUtils.h"

// from DELPHES
#include "modules/Delphes.h"
#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesHepMCReader.h"
#include "ExRootAnalysis/ExRootConfReader.h"
// from ROOT
#include "TDatabasePDG.h"

// local
#include "DelphesAlg.h"

// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

//from ROOT
#include "TROOT.h"
#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"
#include "TObjArray.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include "GaudiKernel/KeyedTraits.h"

// ============================================================================
// Relations 
// ============================================================================
//#include "Relations/Relation1D.h"
// ============================================================================


//-----------------------------------------------------------------------------
// Implementation file for class : DelphesAlg
//
// 2015-01-19 : Patrick Robbe
//-----------------------------------------------------------------------------


using namespace std;
// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( DelphesAlg )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DelphesAlg::DelphesAlg( const std::string& name,
                        ISvcLocator* pSvcLocator)
: GaudiAlgorithm ( name , pSvcLocator )
{
  declareProperty("HepMCEventLocation",
                  m_generationLocation = LHCb::HepMCEventLocation::Default,
                  "Location to read the HepMC event.");
  declareProperty("MCParticleLocation",
                  m_generatedParticles = LHCb::MCParticleLocation::Default,
                  "Location to write generated MCParticles.");
  declareProperty("MCVerticesLocation",
                  m_generatedVertices = LHCb::MCVertexLocation::Default,
                  "Location to write generated MCVertices.");
  declareProperty("MCFastParticleLocation",
                  m_particles = "/Event/MCFast/MCParticles",
                  "Location to write smeared MCParticles.");
  declareProperty("MCFastVerticesLocation",
                  m_vertices = "/Event/MCFast/MCVertices",
                  "Location to write smeared MCVertices.");
  
  declareProperty("LHCbDelphesCardLocation",
                  m_LHCbDelphesCard = "./cards/delphes_card_LHCb.tcl",
                  "Location of the Delphes Card for specific LHCb Implementation");
  declareProperty("DelphesTrackLocation",m_track_location="TrackMerger/tracks",
                  "Location of the merged track (and neutral) candidates");
  
                  
}
//=============================================================================
// Destructor
//=============================================================================
DelphesAlg::~DelphesAlg() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode DelphesAlg::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // gROOT->SetBatch();
  key_gen = 0;
  m_confReader = new ExRootConfReader;
  m_confReader->ReadFile(m_LHCbDelphesCard.c_str());
  
  m_modularDelphes = new Delphes("Delphes");
  m_modularDelphes->SetConfReader(m_confReader);
  
  m_Delphes = m_modularDelphes->GetFactory();
  
  m_allParticleOutputArray = m_modularDelphes->ExportArray("allParticles");
  m_stableParticleOutputArray = m_modularDelphes->ExportArray("stableParticles");
  m_partonOutputArray = m_modularDelphes->ExportArray("partons");
  
  m_modularDelphes->InitTask();

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DelphesAlg::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  m_modularDelphes->Clear();

  // convert hep mc -> Delphes
  LHCb::HepMCEvents* generationEvents = 
    get<LHCb::HepMCEvents>(m_generationLocation);

  //Get TES container
  LHCb::MCParticles *m_genParticleContainer = nullptr;
  LHCb::MCVertices  *m_genVerticesContainer = nullptr;

  LHCb::MCParticles *m_particleContainer = nullptr;
  LHCb::MCVertices  *m_verticesContainer = nullptr;
 
  //make the generated from hepmc particle container if it doesn't exist
  if(exist<LHCb::MCParticles>(m_generatedParticles))
    m_genParticleContainer = get<LHCb::MCParticles>(m_generatedParticles);
  else {
    m_genParticleContainer = new LHCb::MCParticles();
    put(m_genParticleContainer, m_generatedParticles);
  }
  ///vertices don't exist yet, make them
  if(exist<LHCb::MCVertices>(m_generatedVertices))
    m_genVerticesContainer = get<LHCb::MCVertices>(m_generatedVertices);
  else {
    m_genVerticesContainer = new LHCb::MCVertices();
    put(m_genVerticesContainer, m_generatedVertices);
  }
  //get particle container of LHCb MC Particles
  if(exist<LHCb::MCParticles>(m_particles))
    m_particleContainer = get<LHCb::MCParticles>(m_particles);
  else {
    m_particleContainer = new LHCb::MCParticles();
    put(m_particleContainer, m_particles);
  }
  //ditto on the vertices
  if(exist<LHCb::MCVertices>(m_vertices))
    m_verticesContainer = get<LHCb::MCVertices>(m_vertices);
  else {
    m_verticesContainer = new LHCb::MCVertices();
    put(m_verticesContainer, m_vertices);
  }
  
  
  
  Candidate *candidate;
  
  TDatabasePDG *pdg;
  TParticlePDG *pdgParticle;
  Int_t pdgCode;

  Int_t pid( 0 ) , status( 0 );
  //std::vector<HepMC::GenVertex> generated_pvs;
  std::vector<int>generated_PV_barcodes;  
  pdg = TDatabasePDG::Instance();
  //loop over all HepMC Events
  for(LHCb::HepMCEvents::const_iterator genEvent = generationEvents->begin(); 
      generationEvents->end() != genEvent; ++genEvent) {
    debug()<<"----------From HepMC Event----------------"<<endmsg;
    debug()
      <<"PID"<<setw(12)
      <<"Status"<<setw(12)
      <<"Px"<<setw(12)
      <<"Py"<<setw(12)
      <<"Pz"<<setw(12)
      <<"Mass"<<setw(12)
      <<"Energy"<<setw(12)
      <<"x"<<setw(12)
      <<"y"<<setw(12)
      <<"z"<<setw(12)
      <<"M1"<<setw(12)
      <<"MCKey"<<setw(12)
      <<"Eta"
      <<endmsg ;
    //Retrieve the event.
    
    
    HepMC::GenEvent* ev = (*genEvent)->pGenEvt();
    //put pointer to PV
    //stolen from GenerationToSimulation
    HepMC::GenVertex* genPV = primaryVertex(ev);    
    if ( msgLevel(MSG::DEBUG) ) debug()<<"got generated PV"<<genPV->barcode()<<endmsg;
    generated_PV_barcodes.push_back(genPV->barcode());
    
    //loop over particles in hep mc event
    for (HepMC::GenEvent::particle_const_iterator itP = ev->particles_begin();
         itP != ev->particles_end(); ++itP) {
      //make delphes candidate
      candidate = m_Delphes->NewCandidate();
      
      candidate->PID = (*itP) -> pdg_id() ;
      pid = candidate -> PID ;
      if ( msgLevel(MSG::DEBUG) ) debug()<<"now on candidate with PID "<<pid<<endmsg;
      if ( msgLevel(MSG::DEBUG) ) debug()<<"from hepMC particle, mass = "<<(*itP)->generated_mass () <<", and from momentum "<<(*itP)->momentum().m()<<endmsg;
      
      pdgCode = TMath::Abs(candidate->PID);
      
      candidate->Status = (*itP) -> status() ;
      status = candidate -> Status ;
      
      pdgParticle = pdg->GetParticle(pid);
      candidate->Charge = pdgParticle ? Int_t(pdgParticle->Charge()/3.0) : -999;
      candidate->Mass = (*itP) -> generated_mass() ;
      
      candidate->Momentum.SetPxPyPzE((*itP) -> momentum().x() , 
                                     (*itP) -> momentum().y() , 
                                     (*itP) -> momentum().z(), 
                                     (*itP) -> momentum().t() ) ;

      debug()<<"does the particle have a production vertex? "<<(*itP)->production_vertex() <<endmsg;      
      
      if ( 0 != (*itP)->production_vertex() ) 
      {
        //zehua here adds that if there's a mother, get the mother's unique ID for later use
        candidate->Position.SetXYZT( (*itP) -> production_vertex()->position().x() , 
                                     (*itP) -> production_vertex()->position().y() , 
                                     (*itP) -> production_vertex()->position().z() , 
                                     (*itP) -> production_vertex()->position().t() ) ;
        // for(HepMC::GenVertex::particle_iterator mother = (*itP)->production_vertex()->particles_begin(HepMC::parents); 
        //     mother!=(*itP)->production_vertex()->particles_end(HepMC::parents);
        //     ++mother ){	
        //   if ( msgLevel(MSG::DEBUG) ) debug() << " mother ID: " << (*mother)->pdg_id() 
        //          << "\t mother barcode: " << (*mother)->production_vertex()->barcode()
        //          << "\t mother info: "<<endmsg;          
        //   (*mother)->print();
          
          
        // }
        
      }
      const TLorentzVector &mc_momentum = candidate->Momentum;
      //candidate->M2 = key_gen;
      candidate->M2 = genPV->barcode();//assign M2 to have the barcode of the PV.    
      candidate->SetUniqueID(key_gen);
      //set the info of the candidate particle      
      candidate->M1 = key_gen;
      LHCb::MCParticle *MCpart = new LHCb::MCParticle();
      LHCb::ParticleID MCpartID;
      MCpartID.setPid(candidate->PID);
      MCpart->setParticleID(MCpartID);
      Gaudi::LorentzVector gaudi_momentum;
      Double_t modMCP2 = TMath::Power(mc_momentum.Px(),2) + TMath::Power(mc_momentum.Py(),2) + TMath::Power(mc_momentum.Pz(),2); 
      Double_t enMC = TMath::Sqrt(modMCP2 + TMath::Power(candidate->Mass,2));
      gaudi_momentum.SetPxPyPzE(mc_momentum.Px(),mc_momentum.Py(),mc_momentum.Pz(),enMC);
      const TLorentzVector &position= candidate->Position;
      // gaudi_momentum.SetXYZT(position.X(),
      //                        position.Y(),
      //                        position.Z(),
      //                        position.T());
      char num1[50],num2[50],num3[50],num4[50];
      sprintf(num1,"%.8f",candidate->Momentum.Px());
      sprintf(num2,"%.8f",candidate->Momentum.Py());
      sprintf(num3,"%.8f",candidate->Momentum.Pz());
      sprintf(num4,"%.8f",candidate->Momentum.E());
      if ( msgLevel(MSG::DEBUG) ) debug()<<"for particle pid"<<candidate->PID<<" and m "<<candidate->Mass
            <<", (px,py,pz,E) = ("//<<candidate->Momentum.Px()<<","<<candidate->Momentum.Py()<<","<<candidate->Momentum.Pz()<<","<<candidate->Momentum.E()
            <<num1<<","<<num2<<","<<num3<<","<<num4
            <<")"<<endmsg;

      MCpart->setMomentum(gaudi_momentum);
      

      Gaudi::XYZPoint point(position.X(),
                            position.Y(),
                            position.Z());

      //set MC vertex position.
      LHCb::MCVertex *origVtx = new LHCb::MCVertex();
      
      origVtx->setPosition(point);
      origVtx->setTime(position.T());

      m_genVerticesContainer->insert(origVtx);
      // debug()<<"comparing vertices"<<endmsg;   
            //if ( msgLevel(MSG::DEBUG) ) debug()<<"got production vertex"<<(*itP) -> production_vertex()->barcode()<<endmsg;
      
      //if((*itP) -> production_vertex()->barcode()==candidate->M2)
      // {
        
      //   if ( msgLevel(MSG::DEBUG) ) debug()<<"found PV at ("<<point.X()<<","<<point.Y()<<","<<point.Z()<<")"<<endmsg;        
      //   origVtx->setType(LHCb::MCVertex::ppCollision);
      //   if ( msgLevel(MSG::DEBUG) ) debug()<<"testing PV->isPrimary()"<<origVtx->isPrimary()<<endmsg;
      // }
      // else{origVtx->setType(LHCb::MCVertex::GenericInteraction);}
            
      MCpart->setOriginVertex(origVtx);
      
      m_genParticleContainer->insert(MCpart,key_gen);
      key_gen++;

      Double_t eta  = MCpart->pseudoRapidity();
      
      //Gen[key_gen] = MCpart;
      debug()
        <<candidate->PID<<setw(12)
        <<candidate->Status<<setw(12)
        <<candidate->Momentum.Px()<<setw(12)
        <<candidate->Momentum.Py()<<setw(12)
        <<candidate->Momentum.Pz()<<setw(12)
        <<candidate->Mass<<setw(12)
        <<enMC<<setw(12)
        <<candidate->Position.X()<<setw(12)
        <<candidate->Position.Y()<<setw(12)
        <<candidate->Position.Z()<<setw(12)
        <<candidate->M1<<setw(12)
        <<MCpart->key()<<setw(12)
        <<eta
        <<endmsg ;
      
      m_allParticleOutputArray->Add(candidate);
   
      if(!pdgParticle) continue ;
      
      if((status == 1) || (status==999))
      {
        debug()<<"adding candidate PID "<<candidate->PID<<" to stable particles"<<endmsg;
        
        m_stableParticleOutputArray->Add(candidate);
      }
      else if(pdgCode <= 5 || pdgCode == 21 || pdgCode == 15)
      {
        m_partonOutputArray->Add(candidate);
      }
    }
  }//end loop on generated particles and the HepMC event
  
  debug()<<"processing task in delphes"<<endmsg;  
  m_modularDelphes->ProcessTask();//make delphes talk
  ///get tracks from delphes
  const TObjArray *arrayTracks = m_modularDelphes->ImportArray(m_track_location.c_str());

  TIter iteratorTracks(arrayTracks);
  
  iteratorTracks.Reset();
  
  
  debug()<<"----------From TrackMerger----------------"<<endmsg;
  debug()
    <<"PID"<<setw(12)
    <<"Status"<<setw(12)
    <<"Px"<<setw(12)
    <<"Py"<<setw(12)
    <<"Pz"<<setw(12)
    <<"Mass"<<setw(12)
    <<"Energy"<<setw(12)
    <<"x"<<setw(12)
    <<"y"<<setw(12)
    <<"z"<<setw(12)
    <<"t"<<setw(12)
    <<"x_0"<<setw(12)
    <<"y_0"<<setw(12)
    <<"z_0"<<setw(12)
    <<"t_0"<<setw(12)
    <<"M1"<<setw(12)
    <<"Eta"<<setw(12)
    <<"M2"
    <<endmsg ;
  
  
  iteratorTracks.Reset();
  Candidate *particle;
  Candidate *mother;
  Candidate *first_particle;
  //loop over tracks
  while((mother = static_cast<Candidate*>(iteratorTracks.Next())))
  {
    debug()<<"status = of this mother: "<<mother->Status<<endmsg;
    bool isAlreadyContained = false;        
    //mother here is the mother of the object, not the mother in the decay.
    debug()<<"checking for time hits:"<<mother->NTimeHits<<endmsg;
    if(!(mother->Status==1 || mother->Status==999)){
      debug()<<"Now considering Mother PID "<<mother->PID<<", Key"<<mother->M1<<", UniqueID "<<mother->GetUniqueID()<<endmsg;
      
      
      //propbably this key is already in the container, and it's a duplicate charged track from the calo
      //look explicitly for it
      int sizeOfCandidates = mother->GetCandidates()->GetEntries();
      debug()<<"there are "<<sizeOfCandidates<<" candidates to consider"<<endmsg;      
      
      Candidate* candUp;
      TIter itMother(mother->GetCandidates());      
      while(candUp=static_cast<Candidate*>(itMother.Next())){
        long PartKey = ((Candidate*)candUp)->M1;
        int PartPID = ((Candidate*)candUp)->PID;
        debug()<<"looping on candidates in mother, production vertex = "<<candUp->M2<<", with PID "<<PartPID<<endmsg;
        // if(PartPID==22){
          
        //   debug()<<"(px,py,pz,E) = ("<<candUp->Momentum.Px()<<","<<candUp->Momentum.Py()<<","<<candUp->Momentum.Pz()<<","<<candUp->Momentum.E()<<")"<<endmsg;
          
        // }
        
        debug()<<"\t Key "<<PartKey<<", PID "<<PartPID<<"uniqueID "<< candUp->GetUniqueID()
              <<", contained: "<<m_particleContainer->containedObject(PartKey)<<endmsg;
        isAlreadyContained = m_particleContainer->containedObject(PartKey)!=0;
        if(isAlreadyContained){break;}
      }
      if(isAlreadyContained){
        debug()<<"candidate already written. moving on"<<endmsg;
        continue;          
      }
      //continue;
      
    }
    
    // particle = static_cast<Candidate*>(mother->GetCandidates()->At(0));
    // auto arrayPart = mother->GetCandidates();
    // particle = (Candidate*)arrayPart->Last();
    particle = (Candidate*)mother;
    first_particle = (Candidate*)mother->GetCandidates()->First();
    debug()<<"status = of the first particle: "<<first_particle->Status<<endmsg;
    const TLorentzVector &momentum = particle->Momentum;
    //Convert delphes particles into MCParticles
    LHCb::MCParticle *part = new LHCb::MCParticle();
    LHCb::ParticleID partID;
    partID.setPid(particle->PID);
    part->setParticleID(partID);
    Gaudi::LorentzVector m;
    Double_t modP2 = TMath::Power(momentum.Px(),2) + TMath::Power(momentum.Py(),2) + TMath::Power(momentum.Pz(),2); 
    Double_t en = TMath::Sqrt(modP2 + TMath::Power(particle->Mass,2));
    // m.SetPxPyPzE(momentum.Px(),momentum.Py(),momentum.Pz(),en);
    m.SetPxPyPzE(momentum.X(),momentum.Y(),momentum.Z(),momentum.T());
    const TLorentzVector &position = particle->Position;
    const TLorentzVector &first_position = first_particle->Position;
    // m.SetXYZT(position.X(),
    //           position.Y(),
    //           position.Z(),
    //           position.T());
    
    part->setMomentum(m);
    
    size_t key_rec = particle->M1;
    

    Gaudi::XYZPoint point;
    point.SetXYZ(position.X(),
                 position.Y(),
                 position.Z());
    
    LHCb::MCVertex *EndVtx = new LHCb::MCVertex();
    EndVtx->setPosition(point);
    EndVtx->setTime(position.T());
    EndVtx->setType( LHCb::MCVertex::DecayVertex);
    
    Double_t eta = part->pseudoRapidity();

    Gaudi::XYZPoint first_point;
    first_point.SetXYZ(first_position.X(),
		       first_position.Y(),
		       first_position.Z());

    LHCb::MCVertex *OrigVtx = new LHCb::MCVertex();
    OrigVtx->setPosition(first_point);
    OrigVtx->setTime(first_position.T());    
    if(std::find(generated_PV_barcodes.begin(), generated_PV_barcodes.end(), candidate->M2) != generated_PV_barcodes.end()){
      debug()<<"setting pv at the origin vertex insertion"<<endmsg;
      
      OrigVtx->setType( LHCb::MCVertex::ppCollision);
    }
    else{
      OrigVtx->setType( LHCb::MCVertex::GenericInteraction);
    }
    
    
    
    debug()
      <<particle->PID<<setw(12)
      <<particle->Status<<setw(12)
      <<particle->Momentum.Px()<<setw(12)
      <<particle->Momentum.Py()<<setw(12)
      <<particle->Momentum.Pz()<<setw(12)
      <<particle->Mass<<setw(12)
      <<en<<setw(12)
      <<particle->Position.X()<<setw(12)
      <<particle->Position.Y()<<setw(12)
      <<particle->Position.Z()<<setw(12)
      <<position.T()<<setw(12)
      <<first_particle->Position.X()<<setw(12)
      <<first_particle->Position.Y()<<setw(12)
      <<first_particle->Position.Z()<<setw(12)
      <<first_position.T()<<setw(12)
      <<particle->M1<<setw(12)
      <<eta
      <<endmsg ;
    
    m_verticesContainer->insert(OrigVtx);
    m_verticesContainer->insert(EndVtx);
    // part->setOriginVertex(EndVtx);
    part->addToEndVertices(EndVtx);
    part->setOriginVertex(OrigVtx);
    debug()<<"putting particle into the container"<<endmsg;
    
    m_particleContainer->insert(part,key_rec); 
    
  }
  

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode DelphesAlg::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  
  m_modularDelphes->FinishTask();
  delete m_modularDelphes;
  delete m_confReader; 

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
 
//=============================================================================
HepMC::GenVertex* DelphesAlg::primaryVertex
(const HepMC::GenEvent* genEvent) const {
  HepMC::GenVertex* result = nullptr;  
  // First method, get the beam particle and use the decay vertex if it exists.
  if (genEvent->valid_beam_particles()) {
    HepMC::GenParticle* P = genEvent->beam_particles().first;
    HepMC::GenVertex*   V = P->end_vertex();
    if (V) result = V;    
    else error() << "The beam particles have no end vertex!" << endmsg;
  // Second method, use the singal vertex stored in HepMC.
  } else if ( 0 != genEvent -> signal_process_vertex() ) {
    HepMC::GenVertex* V = genEvent->signal_process_vertex();
    result = V;    
  // Third method, take production/end vertex of the particle with barcode 1.
  } else {
    HepMC::GenParticle* P = genEvent->barcode_to_particle(1);
    HepMC::GenVertex*   V = 0;
    if (P) {
      V = P->production_vertex();
      if (V) result = V;      
      else {
        V = P->end_vertex();
        if (V) result = V;        
        else error() << "The first particle has no production vertex and "
		     << "no end vertex !" << endmsg ;
      }
    } else error() << "No particle with barcode equal to 1!" << endmsg;
  }
  return result;
}
