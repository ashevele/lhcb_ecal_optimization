#pragma once
// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Particle.h"
#include "GaudiKernel/RndmGenerators.h"//for MC integration
//calo event
#include "Event/CaloHypo.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CaloPosition.h"
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "Event/CaloCluster.h"
#include "Kernel/CaloCellCode.h"//for talking back and forth between conventions
//c++
#include <tuple>
#include <map>
#include <string>
#include <fstream>


class ExRootConfReader ;
class Delphes ;
// class DelphesFactory ;
// class TObjArray ;
//define some classes for more useful info than std::tuples
/** @class DelphesEcalTower DelphesProto.h
 *  primitive holder for ECAL cell
 *
 *  @author Adam Davis
 *  @date   2018-6-1
 */
class DelphesEcalTower{
public:
  DelphesEcalTower(double cellxsize,double cellysize,std::string region,int row,int col, float energy){
    m_cellXsize = cellxsize;
    m_cellYsize = cellysize;
    
    m_region = region;
    m_row = row;
    m_col = col;
    m_energy = energy;
    
  }
  // bool operator==(const DelphesEcalTower a, const DelphesEcalTower b){
  //   return (a.Region()==b.Region() 
  //           && a.Row()==b.Row() 
  //           && a.Col()==b.Col()
  //           && a.E()==b.E());
    
  // }
  
  std::string Region(){return m_region;}
  int Row(){return m_row;}
  int Col(){return m_col;}
  float E(){return m_energy;}
  void SetE(float e){m_energy = e;}
  void SetTimeInfo(int t,int tmin,int tmax){
    m_time = t;
    m_timemin = tmin;
    m_timemax = tmax;
  }
  void SetCellXsize(double max_x_size){m_cellXsize = max_x_size;}
  void SetCellYsize(double max_y_size){m_cellYsize = max_y_size;}
  double GetCellXsize(){return m_cellXsize;}
  double GetCellYsize(){return m_cellYsize;}
    
    
private:
  std::string m_region;
  uint m_row;
  uint m_col;
  float m_energy;  
  //size of cell for ease
  double m_cellXsize;
  double m_cellYsize;
  
  //for later
  int m_time;
  int m_timemin;
  int m_timemax;
  
};

/** @class DelphesSimEcalCluster DelphesProto.h
 *  group of towers with necessary MC info
 *
 *  @author Adam Davis
 *  @date   2018-6-1
 */
class DelphesSimEcalCluster{
  //placeholder for delphes info
public:
  //constructor  
  DelphesSimEcalCluster(std::string seedreg, int seedrow, int seedcol){
    m_seedreg = seedreg;
    m_seedrow = seedrow;
    m_seedcol = seedcol;

    
  };
  //define == operator
  // bool operator==(const DelphesSimEcalCluster a, const DelphesSimEcalCluster b){
  //   return (a.SeedRegion==b.SeedRegion
  //           && a.SeedRow()==b.SeedRow()
  //           && a.SeedCol()==b.SeedCol()
  //           && (
  std::string SeedRegion(){return m_seedreg;}
  uint SeedRow(){return m_seedrow;}
  uint SeedCol(){return m_seedcol;}
  void SetSeedEnergy(float energy){m_seedEnergy = energy;}
  float SeedEnergy(){return m_seedEnergy;}
  
  void AddTower(double cellxsize,double cellysize,std::string region, int row, int col, float energy){
    m_towers.push_back(DelphesEcalTower(cellxsize,cellysize,region,row,col,energy));}
  
  // void RemoveTower(std::string region, int row, int col){
  //   //find the cell in m_towers
  
  //   std::vector<DelphesEcalTower>::iterator i = std::find_if(m_towers.begin(), m_towers.end(),
  //                                                            [&](const DelphesEcalTower& x) {
  //                                                              return (x.Region() == region &&
  //                                                                      x.Row() == row() &&
  //                                                                      x.Col() == col());
  //                                                            }
  //                                                            );
  //   if(!i==m_towers.end()){
  //     //found the tower.
  //     m_towers.erase(i);      
  //   }
  //   else{warning()<<"cannot remove non-existing tower!"<<endmsg;
  //     return;      
  //   }
  // }
  
  
  std::vector<DelphesEcalTower> Towers(){return m_towers;}
  void UpdateEnergy(std::string region, int row, int col, float newEnergy){
    //find tower matching region,row,col and update energy
    //good for spillover.
    //can do better than a loop here.
    for(auto t : m_towers){
      if(t.Region()==region && t.Row()==row &&t.Col()==col)t.SetE(newEnergy);
      break;      
    }
  }
  float ClusterEnergy(){
    if(m_energy==-1){//recompute if uninitialized
      m_energy=0;
      for(auto t : m_towers){
        m_energy+=t.E();
      }
    }
    return m_energy;
  }
  void SetMCKey(int key){m_MCKey = key;}
  int MCKey(){return m_MCKey;}
  void SetMCEnergy(float e){m_mc_energy = e;}
  float MCEnergy(){return m_mc_energy;}
  
  
private:
  int m_MCKey;  
  std::string m_seedreg;
  int m_seedrow;
  int m_seedcol;
  float m_seedEnergy;  
  float m_energy{-1};  
  float m_mc_energy;
  
  std::vector<DelphesEcalTower> m_towers;
};

typedef std::vector<DelphesSimEcalCluster> DelphesSimEcalClusters;

  
/** @class DelphesProto DelphesProto.h
 *  
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2015-11-26
 *  @date   2018-1-29 add reader for calo geometry
 */
class DelphesProto : public GaudiAlgorithm {
public: 
  /// Standard constructor
  DelphesProto( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DelphesProto( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization
  
  //  std::tuple<LHCb::ProtoParticle,int> NeutralProtoFromDelphes( const LHCb::MCParticle part );
protected:
  std::vector<double> AddInfo(double oneOverP, std::vector<std::vector<double>> lutMatrix);
  
private:
  
  Rndm::Numbers m_flatDist ;  ///< Flat random number generator
  Rndm::Numbers m_gaussDist ;//Gaussian random number generator

  std::string m_particles; ///< Location in TES of output smeared MCParticles.
  std::string m_vertices;  ///< Location in TES of output smeared MCVertices.
  std::string m_generatedParticles;  //< Location in TES of output generated MCParticles.
  std::string m_generatedVertices;  //< Location in TES of output generated MCVertices.
  std::string m_protoParticles;  //< Location in TES of output ProtoParticles
  std::string m_tracks; //< Location in TES of output tracks
  std::string m_richPIDs; //< Location in TES of output RichPIDs

  std::string m_trackLUT;//Track lookup table (ghostProp, likelihood...)
  std::string m_CovarianceLUT;//Covariance Matrix lookup table
  
  std::vector<std::vector<double>> lutTrackMatrix; //Matrix table for tracks quantities
  bool existTrackLUT;//check if lookup table exists
  std::vector<std::vector<double>> lutCovarianceMatrix;//Covariance Matrix table 
  bool existCovarianceLUT;//check if covariance matrix lut exists



  std::string m_neutralLocation;// location in TES of output neutrals 
  std::string m_photonsLocation;
  std::string m_caloDigitsLocation;
  
  std::string m_caloClusterLocation;
  

  ///stuff for delphes calo reading
  double m_zEcal;
  int m_CaloClusterLoopSize;
  int m_num_int_pts;
  
  std::string m_LHCbDelphesCard;
  //make a call-able object to
  std::map<std::string,
           std::map<std::string,
                    std::map<std::string,double> 
                    > 
           > m_calo_spill_boundaries;  //x,y,cellsize
  std::vector<double> m_calo_x_borders;
  std::vector<double> m_calo_y_borders;
  std::vector<double> m_calo_x_bins;
  std::vector<double> m_calo_y_bins;//asssumes no crazy shapes, just a grid.
  int m_cellCenterX;//central cell for CALO in X, assume 32 for LHCb Run I and II geo
  int m_cellCenterY;//central cell for CALO in Y, assume 32 for LHCb Run I and II geo
  ICaloHypoEstimator* m_estimator = nullptr;
  float m_moliere_radius;
  //intersection for moliere radius and calo cell
  std::vector<std::tuple<float,float> > PointsOfIntersection(float xhit, float yhit, 
                                                             float xmin, float xmax, 
                                                             float ymin, float ymax, float rM);
  
  float EnergyFraction(float x1, float x2, float y1, float y2, float num_RM, int num_integration_points =1000);

  std::vector<std::string>m_RegionNames;//pointer to names of regions.
  
  bool is_inside(float box_x1, float box_y1, float box_x2, float box_y2, float x, float y){
    debug()<<"now texting box with (x1,y1)--(x2,y2) ("<<box_x1<<","<<box_y1<<")--("<<box_x2<<","<<box_y2<<")"<<endmsg;
    debug()<<"point to test is ("<<x<<","<<y<<")"<<endmsg;
    
    if (x < box_x1 || x > box_x2) return 0;
    if (y < box_y1 || y > box_y2) return 0;
    return 1;
  }

  
  //vectors of objects to be written which don't go out of scope
  // std::vector<LHCb::CaloCellID> m_cellIDs;
  // std::vector<LHCb::CaloDigit> m_digits;
  // std::vector<LHCb::CaloHypo> m_hypos;
  // std::vector<LHCb::ProtoParticle> m_protos;
  // std::vector<LHCb::CaloPosition> m_cpos;
};

