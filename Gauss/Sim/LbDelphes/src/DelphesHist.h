#ifndef DELPHESHIST_H 
#define DELPHESHIST_H 1

// Include files 
// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiHistoTool.h"

/** @class DelphesHist DelphesHist.h
 *  
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2015-10-20
 */
class DelphesHist : public GaudiHistoAlg {
public: 
  /// Standard constructor
  DelphesHist( const std::string& name, ISvcLocator* pSvcLocator );

  virtual ~DelphesHist( ); ///< Destructor

  virtual StatusCode initialize();    ///< Algorithm initialization
  virtual StatusCode execute   ();    ///< Algorithm execution
  virtual StatusCode finalize  ();    ///< Algorithm finalization

protected:

private:
  std::string m_particles;  ///< Location in TES of output smeared MCParticles.                                                                                                     
  std::string m_vertices;///< Location in TES of output smeared MCVertices.                                                                                                         
  std::string m_generatedParticles;//< Location in TES of output generated MCParticles.
  std::string m_generatedVertices;//< Location in TES of output generated MCVertices.                                                                                              
  
  std::map<Long64_t, std::pair<LHCb::MCParticle*,LHCb::MCParticle*>> Pions;
  std::map<Long64_t, LHCb::MCParticle*> GenPions;  
  std::map<Long64_t, LHCb::MCParticle*> RecPions;  
  AIDA::IHistogram1D * m_RGen;
  AIDA::IHistogram1D * m_RRec;
  AIDA::IHistogram1D * m_ThetaGen;
  AIDA::IHistogram1D * m_ThetaRec;
  AIDA::IHistogram1D * m_PhiGen;
  AIDA::IHistogram1D * m_PhiRec;
  AIDA::IHistogram1D * m_EtaGen;
  AIDA::IHistogram1D * m_EtaRec;
  AIDA::IHistogram1D * m_ZGen;
  AIDA::IHistogram1D * m_ZRec;
  AIDA::IHistogram2D * m_XYGen;
  AIDA::IHistogram2D * m_XYRec;
  AIDA::IHistogram2D * m_EtaPhiGen;
  AIDA::IHistogram2D * m_EtaPhiRec;
  AIDA::IHistogram2D * m_P;
  AIDA::IHistogram2D * m_Pt;
  AIDA::IHistogram2D * m_PX;
  AIDA::IHistogram2D * m_PY;
  AIDA::IHistogram2D * m_PZ;
  AIDA::IHistogram2D * m_E;
  AIDA::IHistogram2D * m_M;
  AIDA::IHistogram2D * m_Vertex_X;
  AIDA::IHistogram2D * m_Vertex_Y;
  AIDA::IHistogram2D * m_Vertex_Z;
  AIDA::IHistogram2D * m_PID;
  AIDA::IHistogram2D * m_PhiXGen;
  AIDA::IHistogram2D * m_PhiXRec;
  AIDA::IHistogram2D * m_theta_XY_Gen;
  AIDA::IHistogram2D * m_theta_XY_Rec;
  AIDA::IHistogram2D * m_ThetaP_Gen;
  AIDA::IHistogram2D * m_ThetaP_Rec;
  AIDA::IHistogram3D * m_XYPhiGen;
  AIDA::IHistogram3D * m_XYPhiRec;
  AIDA::IHistogram3D * m_TxTyPGen;
  AIDA::IHistogram3D * m_TxTyPRec;
  
};
#endif // DELPHESHIST_H
