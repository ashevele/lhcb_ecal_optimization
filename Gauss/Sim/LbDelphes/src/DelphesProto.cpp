// Include files 

 // from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"

#include "GaudiKernel/IDataManagerSvc.h"
// Event.
#include "Event/HepMCEvent.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"

// from ROOT
#include "TDatabasePDG.h"
// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
//local
#include "DelphesProto.h"
//from ROOT
#include "TROOT.h"
#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TObjArray.h"
#include "TDatabasePDG.h"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "GaudiKernel/KeyedTraits.h"

//
#include "modules/Delphes.h"
#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesHepMCReader.h"



// ============================================================================
// Relations 
// ============================================================================
//#include "Relations/Relation1D.h"
// ============================================================================


//-----------------------------------------------------------------------------
// Implementation file for class : DelphesProto
//
// 2015-01-19 : Patrick Robbe
// 2017-12-11 : Adam Davis, add neutral protoparticle maker
//-----------------------------------------------------------------------------


using namespace std;
// Declaration of the Algorithm Factory
DECLARE_ALGORITHM_FACTORY( DelphesProto )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DelphesProto::DelphesProto( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator )
{
  declareProperty("MCParticleLocation",
                  m_generatedParticles = LHCb::MCParticleLocation::Default,
  		  "Location to write generated MCParticles.");
  
  declareProperty("MCVerticesLocation",
                  m_generatedVertices = LHCb::MCVertexLocation::Default,
                  "Location to write generated MCVertices.");
  
  declareProperty("MCFastParticleLocation",
                  m_particles = "/Event/MCFast/MCParticles",
  		  "Location to write smeared MCParticles.");
  
  declareProperty("MCFastVerticesLocation",
                  m_vertices = "/Event/MCFast/MCVertices",
                  "Location to write smeared MCVertices.");
  
  declareProperty("MCFastProtoParticles",
                  m_protoParticles = LHCb::ProtoParticleLocation::Charged,
                  "Location to write ProtoParticles.");


  declareProperty("MCFastTrack",
                  m_tracks = LHCb::TrackLocation::Default,
                  "Location to write Tracks.");

  declareProperty("MCFastRichPID",
                  m_richPIDs = LHCb::RichPIDLocation::Default,
                  "Location to write RichPIDs.");

  declareProperty("MCFastNeutralProtoParticles",
                  m_neutralLocation = LHCb::ProtoParticleLocation::Neutrals,
                  "Location to write neutral protoparticles");

  declareProperty("MCFastPhotonsHypo",
                  m_photonsLocation = LHCb::CaloHypoLocation::Photons,
                  "Location to write photon hypothesis");

  declareProperty("MCFastCaloDigits",
                  m_caloDigitsLocation = LHCb::CaloDigitLocation::Default,
                  "Location to wrtie calo psedudigits");

  declareProperty("MCFastCaloClusterLocation",
                  m_caloClusterLocation = LHCb::CaloClusterLocation::Ecal,
                  "Location to write calo clusters");
  
  
  declareProperty("LHCbDelphesCardLocation",
                  m_LHCbDelphesCard = "./cards/delphes_card_LHCb.tcl",
                  "Location of the Delphes Card for specific LHCb Implementation");
  declareProperty("TrackLUT", m_trackLUT = "$LBDELPHESROOT/LookUpTables/lutTrack.dat", "Lookup Table for track quantities");
  declareProperty("TrackCovarianceLUT", m_CovarianceLUT = "$LBDELPHESROOT/LookUpTables/lutCovarianceProf.dat", 
                  "Lookup Table for track covariance matrix");
  declareProperty("CaloCenterX",m_cellCenterX = 32, "central X cell of calo");
  declareProperty("CaloCenterY",m_cellCenterY = 32, "central Y cell of calo");
  declareProperty("MoliereRadius",m_moliere_radius = 35.,"Moliere Radius (in mm)");
  declareProperty("RegionNames",m_RegionNames={"Inner","Middle","Outer"},"Ecal Region names");
  declareProperty("ZEcal",m_zEcal = 12000.,"nominal z position of ecal to consider (front face)");
  declareProperty("CaloClusterSize",m_CaloClusterLoopSize=3,
                  "Number of cells for the calo clusterization to loop over, e.g 3=3x3");
  
  declareProperty("NumericalIntegrationPoints",m_num_int_pts = 1000, 
                  "number of points for calorimeter clusterization numerical integration");
  
  //important. This should be changable and should go away soon.    
}
//=============================================================================
// Destructor
//=============================================================================
DelphesProto::~DelphesProto() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode DelphesProto::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  //random number generator
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_flatDist.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if(sc.isFailure()){
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;    
  }
  sc = m_gaussDist.initialize( randSvc , Rndm::Gauss( 0. , 1. ) ) ;
  if(sc.isFailure()){
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;    
  }
  
  existTrackLUT = false;
  
  info()<<"Loading look-up tables"<<endmsg;  
  info()<<"trying to load track file from "<<m_trackLUT.c_str()<<endmsg;
  
  std::ifstream lutTrackFile(m_trackLUT.c_str());  
  if(lutTrackFile.is_open()){
    info()<<"Loading Track Lookup Table"<<endmsg;   
    existTrackLUT = true;
    std::string line;  
    // while(lutTrackFile.good())
    while(getline(lutTrackFile,line)){
      std::vector<double> vecdummy;
      // getline(lutTrackFile,line);
      std::istringstream isline(line);
      std::string xmin;
      std::string xmax;
      isline>>xmin;
      isline>>xmax;
      // std::cout.precision(20);
      
      // cout<<"xmin: "<<xmin<<"\t xmax: "<<xmax<<endl;
      
      double minv = std::stod(xmin);
      double maxv = std::stod(xmax);
      // cout<<"xminv: "<<minv<<"\t xmaxv: "<<maxv<<endl;
      
      vecdummy.push_back(minv);
      vecdummy.push_back(maxv);
      std::string entry;     
      do{
        isline>>entry;
        vecdummy.push_back(std::stod(entry));
        
      }while(isline.good());
      lutTrackMatrix.push_back(vecdummy);      
    }
    lutTrackFile.close();  
  }
  info()<<"Loaded Track Look-Up table of size"<<lutTrackMatrix.size()<<endmsg;
  //check if actually loaded:
  if(lutTrackMatrix.size()==0){
    warning()<<"No file loaded! existTrackLUT = false!"<<endmsg;
    existTrackLUT=false;
  }
  
  info()<<"trying to load covariance file from "<<m_CovarianceLUT.c_str()<<endmsg;
  
  std::ifstream lutCovarianceFile(m_CovarianceLUT.c_str());
  if(lutCovarianceFile.is_open()){
    info()<<"Loading covariance look-up table"<<endmsg;    
    std::string line;  
    existCovarianceLUT = true;
    
    // while(lutCovarianceFile.good())
    while(getline(lutCovarianceFile,line)){
      std::vector<double> vecdummy;
      // getline(lutCovarianceFile,line);
      std::istringstream isline(line);
      std::string xmin;
      std::string xmax;
      isline>>xmin;
      isline>>xmax;
      // std::cout.precision(20);
      
      // cout<<"xmin: "<<xmin<<"\t xmax: "<<xmax<<endl;
      
      double minv = std::stod(xmin);
      double maxv = std::stod(xmax);
      //cout<<"xminv: "<<minv<<"\t xmaxv: "<<maxv<<endl;
      
      vecdummy.push_back(minv);
      vecdummy.push_back(maxv);
      std::string entry;     
      do{
        isline>>entry;
        vecdummy.push_back(std::stod(entry));
        
      }while(isline.good());
      lutCovarianceMatrix.push_back(vecdummy);      
    }
    lutCovarianceFile.close();  
  }
  info()<<"Loaded covariance lookup table of size "<<lutCovarianceMatrix.size()<<endmsg;
  //add failsafe
  if(lutCovarianceMatrix.size()==0){
    warning()<<"covariance lookup table not found! existCovarianceLUT = false!"<<endmsg;
    existCovarianceLUT=false;   
  }
  
  //m_estimator = tool<ICaloHypoEstimator>("CaloHypoEstimator","CaloHypoEstimator",this);
  
  //get calo configuration from Delphes
  ExRootConfReader *m_confReader = new ExRootConfReader();
  m_calo_x_borders.clear();
  m_calo_y_borders.clear();
  m_calo_x_bins.clear();
  m_calo_y_bins.clear();
    
  m_confReader->ReadFile(m_LHCbDelphesCard.c_str());
  
  //m_modularDelphes = new Delphes("DelphesProto");
  //m_modularDelphes->SetConfReader(m_confReader);
  

  ExRootConfParam XYBins1 = m_confReader->GetParam("Calorimeter::XYBins1");
  m_calo_x_bins.push_back(fabs(XYBins1[0].GetDouble()- XYBins1[2].GetDouble()));
  m_calo_y_bins.push_back(fabs(XYBins1[1][0].GetDouble() - XYBins1[1][1].GetDouble()));  
  ExRootConfParam XYBins2 = m_confReader->GetParam("Calorimeter::XYBins2");
  m_calo_x_bins.push_back(fabs(XYBins2[0].GetDouble()- XYBins2[2].GetDouble()));
  m_calo_y_bins.push_back(fabs(XYBins2[1][0].GetDouble() - XYBins2[1][1].GetDouble()));
  ExRootConfParam XYBins3 = m_confReader->GetParam("Calorimeter::XYBins3");
  m_calo_x_bins.push_back(fabs(XYBins3[0].GetDouble()- XYBins3[2].GetDouble()));
  m_calo_y_bins.push_back(fabs(XYBins3[1][0].GetDouble() - XYBins3[1][1].GetDouble()));
  ExRootConfParam BorderX = m_confReader->GetParam("Calorimeter::BorderX");
  ExRootConfParam BorderY = m_confReader->GetParam("Calorimeter::BorderY");  
  //std::vector<double>tmp;tmp.reserve(XYBins1.GetSize());  
  
      
  //double size_reg1x = fabs((XYBins1[0]).GetDouble() - (XYBins1[2]).GetDouble());
  //double size_reg1y = fabs((XYBins1[1]).GetDouble() - (XYBins1[3]).GetDouble());
  
  info()<<"Got Calorimeter borders X"<<endmsg;
  for(int i=0; i<BorderX.GetSize(); ++i){
    debug()<<"\t"<<BorderX[i].GetDouble()<<endmsg;    
    m_calo_x_borders.push_back(BorderX[i].GetDouble());    
  }
  info()<<"Got Calorimeter borders Y"<<endmsg;
  for(int i=0; i<BorderY.GetSize(); ++i){
    debug()<<"\t"<<BorderY[i].GetDouble()<<endmsg;    
    m_calo_y_borders.push_back(BorderY[i].GetDouble());
  }
  info()<<"Got corresponding cell sizes for region"<<endmsg;
  for(int i=0; i<m_calo_x_bins.size();++i){
    info()<<"\t"<<i<<"-->"<<m_calo_x_bins.at(i)<<"x"<<m_calo_y_bins.at(i)<<endmsg;
  }
  //setting the spill_area_map.
  //how to read this: you pass the current region, Inner, Middle or Outer,
  //then you pass where it spills, either inside or outside, and it gives you 
  //the boundary of that region in x and y.
  //so you will say
  //if( fabs(x)<m_calo_spill_boundaries["Inner"]["Inside"]["X"])... //is inside x
  
  std::map<std::string,std::map<std::string,double> > tmp_map;
  std::map<std::string,double> tmp_inner_map;  
  std::map<std::string,double> tmp_outer_map;  
  //todo, make these configurable 
  //std::vector<std::string> regions = {"Inner","Middle","Outer"};  
  for(uint i=0; i<m_RegionNames.size();++i){ 
    tmp_inner_map["X"] = m_calo_x_borders.at(i);
    tmp_inner_map["Y"] = m_calo_y_borders.at(i);
    tmp_inner_map["CellSizeX"] = (0==i)? 0:m_calo_x_bins.at(i-1);
    tmp_inner_map["CellSizeY"] = (0==i)? 0:m_calo_y_bins.at(i-1);

    tmp_outer_map["X"] = m_calo_x_borders.at(i+1);
    tmp_outer_map["Y"] = m_calo_y_borders.at(i+1);
    tmp_outer_map["CellSizeX"] = (m_RegionNames.size()==i+1)? 0:m_calo_x_bins.at(i+1);
    tmp_outer_map["CellSizeY"] = (m_RegionNames.size()==i+1)? 0:m_calo_y_bins.at(i+1);

    tmp_map["Inside"] = tmp_inner_map;
    tmp_map["Outside"] = tmp_outer_map;
    
    m_calo_spill_boundaries[m_RegionNames.at(i)]= tmp_map;
    debug()<<"test. m_calo_spill_boundaries["<<m_RegionNames.at(i)<<"][\"Inside\"] = "
           <<m_calo_spill_boundaries[m_RegionNames.at(i)]["Inside"]<<endmsg;
  }
  
  
  info()<<"now set spill boundaries to "<<m_calo_spill_boundaries<<endmsg;
  
  //debug()<<"cell size for region 1 = "<<size_reg1x <<" x "<<size_reg1y<<endmsg;
  release( randSvc ) ;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DelphesProto::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  
  
  //Get TES container
  LHCb::ProtoParticles* protoContainer = getOrCreate<LHCb::ProtoParticles,LHCb::ProtoParticles>( m_protoParticles );
  
  LHCb::ProtoParticles* neutralProtoContainer=getOrCreate<LHCb::ProtoParticles,LHCb::ProtoParticles>(m_neutralLocation);
  
  LHCb::RichPIDs* richPIDContainer = getOrCreate<LHCb::RichPIDs,LHCb::RichPIDs>( m_richPIDs );
  
  LHCb::Tracks* trackContainer = getOrCreate<LHCb::Tracks,LHCb::Tracks>( m_tracks );


  LHCb::CaloHypos* caloHypoContainer = getOrCreate<LHCb::CaloHypos,LHCb::CaloHypos>( m_photonsLocation );

  LHCb::CaloDigits* caloDigits = getOrCreate<LHCb::CaloDigits,LHCb::CaloDigits>( m_caloDigitsLocation );
  
  LHCb::MCParticles *m_particleContainer =
    get<LHCb::MCParticles>(m_particles);
  
  LHCb::CaloClusters* caloClusters = getOrCreate<LHCb::CaloClusters,LHCb::CaloClusters>( m_caloClusterLocation );
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Looking at MC Particles size "<<m_particleContainer->numberOfObjects()<<endmsg;
  

  DelphesSimEcalClusters ecal_tower_pos_and_energy;
  //probably best to think about an indivudal class for this... or at least a struct
  
  //loop on MC particles.
  
  LHCb::MCParticles::const_iterator i;
  for( i=m_particleContainer->begin(); i != m_particleContainer->end(); i++ ){
    
    LHCb::MCParticle *part = (*i);
        
    const LHCb::MCVertex *Vtx = part->endVertices()[0];
    const LHCb::MCVertex *OrigVtx = (LHCb::MCVertex*)part->primaryVertex();
    const LHCb::ParticleID &partID = part->particleID();
    debug()<<"processing PID "<<partID.pid()<<endmsg;
    //Charged particles
    
    if(!(partID.pid()==22 || partID.pid()==111 || partID.pid()==2112)){//no photons, pi0s or neutrons
      
      Int_t charge = partID.threeCharge()/3;
      Double_t p = part->momentum().P();
      double qOverP = charge / p;
      
      Double_t px = part->momentum().Px();
      Double_t py = part->momentum().Py();
      Double_t pz = part->momentum().Pz();
      Double_t tx = px/pz;
      Double_t ty = py/pz;

      const Gaudi::XYZPoint &position = Vtx->position();
      const Gaudi::XYZPoint &orig_position = OrigVtx->position();
  
      LHCb::ProtoParticle *proto = new LHCb::ProtoParticle;
      LHCb::Track *track = new LHCb::Track(part->key());
      if(existTrackLUT)
      {        
        std::vector<double> addedInfo = AddInfo(1./p,lutTrackMatrix);        
        

        double chi2 = m_gaussDist()*addedInfo[1]+addedInfo[0];        
        int nDoF = m_gaussDist()*addedInfo[3]+addedInfo[2];
        double ghostProb = m_gaussDist()*addedInfo[5]+addedInfo[4];
        double likelihood = m_gaussDist()*addedInfo[7]+addedInfo[6];

        track->setFitStatus(LHCb::Track::Fitted);
        track->setHistory(LHCb::Track::TrackIdealPR);
        track->setChi2PerDoF(chi2);
        track->setNDoF(nDoF);
        track->setGhostProbability(ghostProb);
        track->setLikelihood(likelihood);
      }
      LHCb::State *trackState = new LHCb::State();
      trackState->setState(position.X(), position.Y(), position.Z(), tx, ty, qOverP);   
      trackState->setLocation(LHCb::State::EndRich2);

      if(existCovarianceLUT)
      {
        Gaudi::TrackSymMatrix cov;
        std::vector<double> addedCovariance = AddInfo(1./p,lutCovarianceMatrix);
        //info()<<"lutCovarianceMatrix = "<<lutCovarianceMatrix<<endmsg;
        
        int icov = 0;
        
        for(int i = 0; i<5; i++)
          for(int j=0; j<5; j++)
          {
            cov(i,j) = m_gaussDist()*addedCovariance[icov+1] + addedCovariance[icov];
            icov++;            
          }
        trackState->setCovariance( cov );
      }
      track->addToStates(*trackState);
      track->setType(LHCb::Track::Long);
      
      
  
      
      LHCb::RichPID* richPID = new LHCb::RichPID();
      const std::vector< float > llValue{-1000.0,0.0,0.0,0.0,0.0,0.0,0.0};

      richPID->setTrack(track);
      switch(abs(partID.pid()))
      {
      case(11):
        richPID->setBestParticleID(Rich::Electron);
        break;
      case(13):
        richPID->setBestParticleID(Rich::Muon);
        break;
      case(211):
        richPID->setBestParticleID(Rich::Pion);
        break;
      case(321):
        richPID->setBestParticleID(Rich::Kaon);
        break;
      case(2212):
        richPID->setBestParticleID(Rich::Proton);
        break;
      default:
        richPID->setBestParticleID(Rich::Unknown);
      }
      proto->addInfo(LHCb::ProtoParticle::NoPID , 1);
      proto->addInfo(LHCb::ProtoParticle::TrackChi2PerDof,1);
      
      
      
      proto->setTrack(track);
  


      
      // if(abs(partID.pid())!= 211) continue;
      richPIDContainer->insert(richPID,part->key());    
      trackContainer->insert(track,part->key());      
      protoContainer->insert(proto,part->key());
    }
    else{//neutrals
      //AD. 28-5-18:
      //need maximum 3 loops here to account for possible pileup effects
      //first loop, form all the cell pairs and the relative energies
      //then loop to find ovelap for pileup
      //then form all the LHCb specific things
      //setup
      //Double_t p = part->momentum().P();
      Double_t px = part->momentum().Px();
      Double_t py = part->momentum().Py();
      Double_t pz = part->momentum().Pz();
      //Double_t tx = px/pz;
      //Double_t ty = py/pz; 
      double orig_energy = sqrt(px*px+py*py+pz*pz);  
      //first, find the particle position at the calo
      //for construction of the LHCb::CaloDigit
      std::string region;
      int row(-1), col(-1);

      Gaudi::XYZPoint center;
      auto v = part->endVertices().back();
      double x =  v->position().x();    
      double y = v->position().y(); 
      double z = v->position().z();       
      debug()<<"checking z = "<<z<<endmsg;
      

      //
      //change border_index, maxYsize, maxXsize to something a bit more descriptive
      
      double maxYsize,maxXsize;
      int numCellsX=-1;
      int numCellsY = -1;
      
      //define the calo region, row and column.
      //from Patrick, for each section, (32,32) is always the center, so define from that.
      //means that not all start at zero!!
      if(z ==m_zEcal){//todo, check this and make it configurable
        //todo make this check a loop.
        if( fabs(x)< m_calo_x_borders.at(0) &&fabs(y)<m_calo_y_borders.at(0)){
          if ( msgLevel(MSG::DEBUG) ) debug()<<"particle in beam pipe"<<endmsg;
          continue;
        }
        //is this in the box for the inner region?
        if(fabs(x)< m_calo_x_borders.at(1) && fabs(y) < m_calo_y_borders.at(1)){
          region = "Inner";
          maxYsize = m_calo_y_bins.at(0);
          maxXsize = m_calo_x_bins.at(0);
          //find the correct row and column of the hit cell.
          numCellsX = m_calo_x_borders.at(1)/maxXsize;//24 for default lhcb geo
          numCellsY = m_calo_y_borders.at(1)/maxYsize;//18 for default lhcb geo
          if ( msgLevel(MSG::DEBUG) ) debug()<<"got numCells (x,y) = ("<<numCellsX<<","<<numCellsY<<") per half"<<endmsg;
        }
        else if(!(fabs(x)< m_calo_x_borders.at(1) && fabs(y) < m_calo_y_borders.at(1))
                &&fabs(x)< m_calo_x_borders.at(2) && fabs(y) < m_calo_y_borders.at(2)){
          region  = "Middle";
          maxYsize = m_calo_y_bins.at(1);
          maxXsize = m_calo_x_bins.at(1);
          //find the correct row and column of the hit cell.
          numCellsX = m_calo_x_borders.at(1)/maxXsize;//24 for default lhcb geo
          numCellsY = m_calo_y_borders.at(1)/maxYsize;//18 for default lhcb geo
          if ( msgLevel(MSG::DEBUG) ) debug()<<"got numCells (x,y) = ("<<numCellsX<<","<<numCellsY<<") per half"<<endmsg;
        }
        else if(!(fabs(x)< m_calo_x_borders.at(2) && fabs(y)< m_calo_y_borders.at(2))&&
                fabs(x)< m_calo_x_borders.at(3) && fabs(y)< m_calo_y_borders.at(3)){
          region = "Outer";
          maxYsize = m_calo_y_bins.at(2);
          maxXsize = m_calo_x_bins.at(2);

        }
        else{
          if ( msgLevel(MSG::DEBUG) ) debug()<<"outside of calo acceptance!!"<<endmsg;
          continue;
          
        }
      }
      else{continue;}
      row = floor(y/maxYsize)+m_cellCenterY;
      col = floor(x/maxXsize)+m_cellCenterX;
      if ( msgLevel(MSG::DEBUG) )
        debug()<<"got end vertex ("<<
          v->position().x()<<" , "<<
          v->position().y()<<" , "<<
          v->position().z()<<" ) "<<endmsg;    
      
      if ( msgLevel(MSG::DEBUG) ) debug()<<"corresponding to (row,col) (" <<row<<","<<col<<")"<<endmsg;

      //std::pair<std::tuple<int,int,int>,std::vector<std::tuple<int,int,int,float> > >ecal_mc_cluster;      
      DelphesSimEcalCluster ecal_mc_cluster(region,row,col);
      ecal_mc_cluster.SetMCKey(part->key());
      
      
      // //generate shower in n moliere radii
      float thisminX = (col -m_cellCenterX)*maxXsize;
      float thismaxX = (col+1 -m_cellCenterX)*maxXsize;
      float thisminY = (row-m_cellCenterY)*maxYsize;
      float thismaxY = (row+1 - m_cellCenterY)*maxYsize;
      if ( msgLevel(MSG::DEBUG) ) debug()<<"checking intersection of hit {"<<x<<","<<y<<"} with box {"
            <<thisminX<<","<<thisminY<<"}--{"<<thismaxX<<","<<thismaxY<<"}"<<endmsg;
      if ( msgLevel(MSG::DEBUG) ) debug()<<"ROOT: TGraph seed; seed.SetPoint(0,"<<x<<","<<y<<");"<<endmsg;
      //if ( msgLevel(MSG::DEBUG) ) debug()<<"ROOT: TBox seedcell("<<thisminX<<","<<thisminY<<","
      //<<thismaxX<<","<<thismaxY<<");"<<endmsg;
      
      //loop over 3 moliere radii to see if we spill into another region
      //int spillRegion = -1;
      //int spillSizeIndex = -1;
      std::string spillRegion = "";      
      int num_moliere_radii = 4;//m_CaloClusterLoopSize
      bool multiRegion = false;
      bool spills_inside_current_region = false;//if it spills and doesn't spill inside, it spills outside      
      //in casting this grid, we can tell if the grid falls into an inner or outer region,
      //defining the behaviour of combining or not combining cells

      for(int i=-1*num_moliere_radii; i<=num_moliere_radii; i++){
        for(int j=-1*num_moliere_radii; j<=num_moliere_radii; j++){
          if ( msgLevel(MSG::DEBUG) ) debug()<<"now on moliere radius point"<<"\t {" <<x + i*m_moliere_radius
                                             <<","<<y + j*m_moliere_radius<<"},"<<endmsg;
          //does this point land in another region?
          //try the same criteria with box
         
          if((fabs(x+i*m_moliere_radius)<m_calo_spill_boundaries[region]["Inside"]["X"]
              && fabs(y+j*m_moliere_radius)<m_calo_spill_boundaries[region]["Inside"]["Y"])&&!multiRegion){
            //spills into region inside
            
            if ( msgLevel(MSG::DEBUG) ) debug()<<"spills into inner region!"<<endmsg;

            multiRegion = true;
            spills_inside_current_region = true;            
            spillRegion = "Inside";            
            
          }
          else if((fabs(x+i*m_moliere_radius)>m_calo_spill_boundaries[region]["Outside"]["X"]
                   || fabs(y+j*m_moliere_radius)>m_calo_spill_boundaries[region]["Outside"]["Y"])
                  &&!multiRegion){//splills into outer region
            if ( msgLevel(MSG::DEBUG) ) debug()<<"spills into outer region!"<<endmsg;
            spillRegion="Outside";
            multiRegion = true;
          }
        }
      }
      //int numInnerCells = m_CaloClusterLoopSize;      
      
      //if ( msgLevel(MSG::DEBUG) ) debug()<< "enter one region only " << endmsg;
      if ( msgLevel(MSG::DEBUG) ) debug()<<"Seed bin is "<<row<<", "<<col<<endmsg;    
      if ( msgLevel(MSG::DEBUG) ) debug()<<"ROOT:TH2D tmp(\"tmp\",\"tmp\","<<m_CaloClusterLoopSize*3+1<<","
            <<((col-m_CaloClusterLoopSize) -m_cellCenterX)*maxXsize<<","
            <<((col+m_CaloClusterLoopSize+1) -m_cellCenterX)*maxXsize<<","<<m_CaloClusterLoopSize*3+1<<","
            <<((row-m_CaloClusterLoopSize) -m_cellCenterY)*maxYsize<<","
            <<((row+m_CaloClusterLoopSize+1) -m_cellCenterY)*maxYsize<<");"<<endmsg;              
      if ( msgLevel(MSG::DEBUG) ) {
        if(multiRegion){
          if(!(("Inner"==region && spills_inside_current_region) || 
               ("Outer"==region && !spills_inside_current_region)) ){
            if ( msgLevel(MSG::DEBUG) ) debug()<<"scale_factorX = "
                                               <<maxXsize/m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]
                                               <<", scale_factorY = "
                                               <<maxYsize/m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]
                                               <<endmsg;
            // if ( msgLevel(MSG::DEBUG) ) debug()<<"Multi region histogram check"<<endmsg;
            if ( msgLevel(MSG::DEBUG) ) debug()<<"ROOT:TH2D spill(\"spill\",\"spill\","
                       <<(m_CaloClusterLoopSize*3+1)*maxXsize/m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]<<","
                                               <<((col-m_CaloClusterLoopSize) -m_cellCenterX)*maxXsize<<","
                       <<((col+m_CaloClusterLoopSize+1) -m_cellCenterX)*maxXsize<<","
                       <<(m_CaloClusterLoopSize*3+1)* maxYsize/m_calo_spill_boundaries[region][spillRegion]["CellSizeY"]
                                               <<","
                                               <<((row-m_CaloClusterLoopSize) -m_cellCenterY)*maxYsize<<","
                                               <<((row+m_CaloClusterLoopSize+1) -m_cellCenterY)*maxYsize<<");"<<endmsg;
          }
        }
      }
      
      
      //int count = 0;
      //iterate around bin.
      for(int i=-1*m_CaloClusterLoopSize; i<=m_CaloClusterLoopSize; i++){//m_CaloClusterLoopSize
        for(int j=-1*m_CaloClusterLoopSize; j<=m_CaloClusterLoopSize; j++ ){//m_CaloClusterLoopSize
          //only difference between same region and multiregion is where the boundaries lie.
          
          auto thiscol = col+i;
          auto thisrow = row+j;
          auto min_x = (thiscol - m_cellCenterX)*maxXsize;
          auto max_x = (thiscol+1 - m_cellCenterX)*maxXsize;
          auto min_y = (thisrow-m_cellCenterY)*maxYsize;
          auto max_y = (thisrow+1 - m_cellCenterY)*maxYsize;
                    
          //now cluster. 
          if ( msgLevel(MSG::DEBUG) ) debug()<<"now in i="<<i<<",j="<<j
                                             <<", borders x ="<<min_x<<","<<max_x<<", y="<<min_y<<","<<max_y<<endmsg;
          
          if(!multiRegion){

            // debug()<<"\t"<< "ListLinePlot[{{"
            //       <<min_x<<","<<min_y<<"},{"
            //       <<min_x<<","<<max_y<<"},{"
            //       <<max_x<<","<<max_y<<"},{"
            //       <<max_x<<","<<min_y<<"},{"
            //       <<min_x<<","<<min_y<<"}}],"<<endmsg;

            //find the associated fraction of points in this cell
            //first find all the points in the cell.
            auto thisfrac1 = EnergyFraction((min_x-x), 
                                            (max_x-x), 
                                            (min_y-y), 
                                            (max_y-y),1., m_num_int_pts);
            auto thisfrac2 = EnergyFraction((min_x-x), 
                                            (max_x-x), 
                                            (min_y-y), 
                                            (max_y-y),2.,m_num_int_pts);
            auto thisfrac3 = EnergyFraction((min_x-x), 
                                            (max_x-x), 
                                            (min_y-y), 
                                            (max_y-y),3.5,m_num_int_pts);
          
            //cout<<"(*cell fraction: 1 Rm: "<<thisfrac1<<", 2 Rm: "<<thisfrac2<<", 3.5 Rm: "<<thisfrac3<<"*)"<<endl;
            if ( msgLevel(MSG::DEBUG) ) debug()<<"tmp.SetBinContent(tmp.FindBin("<<(min_x+max_x)/2.<<","<<(min_y+max_y)/2.<<"),"
                <<thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04<<");"<<endmsg;
            
            //ecal_tower_pos_and_energy.push_back(std::make_tuple(borderIndex,thisrow,thiscol,
            //                                                  orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04),
            //borderIndex,row,col));
            ecal_mc_cluster.AddTower(maxXsize,maxYsize,
                                     region,thisrow,thiscol,orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
            if(thisrow==ecal_mc_cluster.SeedRow() && thiscol==ecal_mc_cluster.SeedCol()){
              ecal_mc_cluster.SetSeedEnergy(orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
              
            }
            
            
          }          
          else{//multiregion
            if ( msgLevel(MSG::DEBUG) ) debug()<<"in multiregion area"<<endmsg;            
            if ( msgLevel(MSG::DEBUG) ) debug()<<"checking inside vs outside criteria"<<endmsg;
            //criteria is we have a cell in some region.
            //is the point we are at in the same region?
            //if it is in the same region, no worries.
            //if it isn't, then adjust cell size appropriately            
            
            
            auto curr_cell_x_center = fabs((min_x+max_x)/2.);
            auto curr_cell_y_center = fabs((min_y+max_y)/2.);            
            bool inspill = is_inside(-1*m_calo_spill_boundaries[region][spillRegion]["X"],//then ask if it is in the box
                                             -1*m_calo_spill_boundaries[region][spillRegion]["Y"],
                                             m_calo_spill_boundaries[region][spillRegion]["X"],
                                             m_calo_spill_boundaries[region][spillRegion]["Y"],
                                             curr_cell_x_center,curr_cell_y_center);
              
            bool is_in_spill_region = spills_inside_current_region?inspill : !inspill;
            //if spills inside, has to be inside, otherwise, has to be out
            if((m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]==0 || 
                m_calo_spill_boundaries[region][spillRegion]["CellSizeY"]==0) && is_in_spill_region){
              if ( msgLevel(MSG::DEBUG) ) debug()<<"no cells and we are in the spill region!! continue!"<<endmsg;
              continue;
            }
            
            
            if ( msgLevel(MSG::DEBUG) ) debug()<<"current cell center (x,y) = ("<<curr_cell_x_center<<","
                                               <<curr_cell_y_center<<")"<<endmsg;
          
            if ( msgLevel(MSG::DEBUG) ) debug()<<"check of boundary logic: spills_inside_current_region = "
                                               <<spills_inside_current_region
                                               <<"is_in_spill_region = "<< is_in_spill_region<<endmsg;
            
            
            
            //first, find out if this row/col is within the right area:
            if ( msgLevel(MSG::DEBUG) ) debug()<<"maxXsize = "<<maxXsize<<", maxYsize = "<<maxYsize<<endmsg;
            if(is_in_spill_region){
              
              if(spills_inside_current_region){
                if(spillRegion=="Inner")continue;//no beampipe
                int new_reg_col = floor((min_x+max_x)/2.)/m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]+m_cellCenterX;
                int new_reg_row = floor((min_y+max_y)/2.)/m_calo_spill_boundaries[region][spillRegion]["CellSizeY"]+m_cellCenterY;
                if ( msgLevel(MSG::DEBUG) ) debug()<<"got new region row,col<<("<<new_reg_row<<","<<new_reg_col<<")"<<endmsg;
                
                //now iterate around this bin
                for(int ii=0; ii<maxXsize/m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]; ++ii){
                  float spillRegminX = (new_reg_col+ii -m_cellCenterX)*m_calo_spill_boundaries[region][spillRegion]["CellSizeX"];
                  float spillRegmaxX = (new_reg_col+1+ii -m_cellCenterX)*m_calo_spill_boundaries[region][spillRegion]["CellSizeX"];
                  for(int jj=0; jj<maxYsize/m_calo_spill_boundaries[region][spillRegion]["CellSizeY"];++jj){
                    if ( msgLevel(MSG::DEBUG) ) debug()<<"now in ii,jj "<<ii<<","<<jj<<endmsg;
                    
                    float spillRegminY = (new_reg_row+jj-m_cellCenterY)*m_calo_spill_boundaries[region][spillRegion]["CellSizeY"];
                    float spillRegmaxY = (new_reg_row+1+jj - m_cellCenterY)*m_calo_spill_boundaries[region][spillRegion]["CellSizeY"];
                    
                    //get the energy fraction and compute.
                    auto thisfrac1 = EnergyFraction((spillRegminX-x), 
                                                    (spillRegmaxX-x), 
                                                    (spillRegminY-y), 
                                                    (spillRegmaxY-y),1.);
                    auto thisfrac2 = EnergyFraction((spillRegminX-x), 
                                                    (spillRegmaxX-x), 
                                                    (spillRegminY-y), 
                                                    (spillRegmaxY-y),2.);
                    auto thisfrac3 = EnergyFraction((spillRegminX-x), 
                                                    (spillRegmaxX-x), 
                                                    (spillRegminY-y), 
                                                    (spillRegmaxY-y),3.5);
                    
                    
                    if ( msgLevel(MSG::DEBUG) ) debug()<<"spill.SetBinContent(spill.FindBin("<<(spillRegminX+spillRegmaxX)/2.<<","<<(spillRegminY+spillRegmaxY)/2.<<"),"
                                                       <<thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04<<");"<<endmsg;
                    ecal_mc_cluster.AddTower(m_calo_spill_boundaries[region][spillRegion]["CellSizeX"],m_calo_spill_boundaries[region][spillRegion]["CellSizeY"],
                                             spillRegion,new_reg_row+jj,new_reg_col+ii,
                                             orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
                    
                  }//jj  
                }//ii
                
              }//inner boundary
              
              else{//outside boundary
                if ( msgLevel(MSG::DEBUG) ) debug()<<"*******************this is an outside boundary case***************************"<<endmsg;
                //cell size is larger, not smaller
                if(spillRegion=="Outer"){
                  if ( msgLevel(MSG::DEBUG) ) debug()<<"no spill outside the calo!"<<endmsg;
                  continue;
                }
                                                
                
                //get division of bins inside
                if ( msgLevel(MSG::DEBUG) ) debug()<<"now merging cells into "<<m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]/maxXsize<<" x "<<m_calo_spill_boundaries[region][spillRegion]["CellSizeY"]/maxYsize<<" bins"<<endmsg;
                //find bin corresponding to this position in the other region.
                int new_reg_col = floor((min_x+max_x)/2.)/m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]+ m_cellCenterX;
                int new_reg_row = floor((min_y+max_y)/2.)/m_calo_spill_boundaries[region][spillRegion]["CellSizeY"]+ m_cellCenterY;
                //if ( msgLevel(MSG::DEBUG) ) debug()<<"got new region row,col<<("<<new_reg_row<<","<<new_reg_col<<")"<<endmsg;
                float spillRegminX = (new_reg_col -m_cellCenterX)*m_calo_spill_boundaries[region][spillRegion]["CellSizeX"];
                float spillRegmaxX = (new_reg_col+1 -m_cellCenterX)*m_calo_spill_boundaries[region][spillRegion]["CellSizeX"];
                float spillRegminY = (new_reg_row -m_cellCenterY)*m_calo_spill_boundaries[region][spillRegion]["CellSizeY"];
                
                float spillRegmaxY = (new_reg_row+1 - m_cellCenterY)*m_calo_spill_boundaries[region][spillRegion]["CellSizeY"];
                if ( msgLevel(MSG::DEBUG) ) debug()<<"spills into outer region"<<endmsg;
                //ask if we already constructed this cell. If so, move on
                bool already_found = false;
                for(auto x : ecal_mc_cluster.Towers())
                {
                  if( x.Region() == spillRegion && x.Row()==new_reg_row && x.Col()==new_reg_col)
                  {
                    if ( msgLevel(MSG::DEBUG) ) debug()<<"already found this one! Continuing!"<<endmsg;
                    already_found = true;
                  }
                }
                if(already_found) continue;
                
                auto thisfrac1 = EnergyFraction((spillRegminX-x), 
                                                (spillRegmaxX-x), 
                                                (spillRegminY-y), 
                                                (spillRegmaxY-y),1.);
                auto thisfrac2 = EnergyFraction((spillRegminX-x), 
                                                (spillRegmaxX-x), 
                                                (spillRegminY-y), 
                                                (spillRegmaxY-y),2.);
                auto thisfrac3 = EnergyFraction((spillRegminX-x), 
                                                (spillRegmaxX-x), 
                                                (spillRegminY-y), 
                                                (spillRegmaxY-y),3.5);
                if ( msgLevel(MSG::DEBUG) ) debug()<<"spill.SetBinContent(spill.FindBin("<<(spillRegminX+spillRegmaxX)/2.<<","<<(spillRegminY+spillRegmaxY)/2.<<"),"
                                                   <<thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04<<");"<<endmsg;
                //ecal_tower_pos_and_energy.push_back(std::make_tuple(spillRegion,new_reg_row,new_reg_col,
                //orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04),
                //borderIndex,row,col));
                ecal_mc_cluster.AddTower(m_calo_spill_boundaries[region][spillRegion]["CellSizeX"],m_calo_spill_boundaries[region][spillRegion]["CellSizeY"],
                                         spillRegion,new_reg_row,new_reg_col,
                                         orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
                
                if ( msgLevel(MSG::DEBUG) ) debug()<<"merging "<<m_calo_spill_boundaries[region][spillRegion]["CellSizeX"]/maxXsize<<" x "<<m_calo_spill_boundaries[region][spillRegion]["CellSizeY"]/maxYsize<<" bins"<<endmsg;
              }
            }            
            else{//normal
              auto thisfrac1 = EnergyFraction((min_x-x), 
                                              (max_x-x), 
                                              (min_y-y), 
                                              (max_y-y),1.);
              auto thisfrac2 = EnergyFraction((min_x-x), 
                                              (max_x-x), 
                                              (min_y-y), 
                                              (max_y-y),2.);
              auto thisfrac3 = EnergyFraction((min_x-x), 
                                              (max_x-x), 
                                              (min_y-y), 
                                              (max_y-y),3.5);
              
              //cout<<"(*cell fraction: 1 Rm: "<<thisfrac1<<", 2 Rm: "<<thisfrac2<<", 3.5 Rm: "<<thisfrac3<<"*)"<<endl;
              if ( msgLevel(MSG::DEBUG) ) debug()<<"tmp.SetBinContent(tmp.FindBin("<<(min_x+max_x)/2.<<","<<(min_y+max_y)/2.<<"),"
                                                 <<thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04<<");"<<endmsg;
              
              ecal_mc_cluster.AddTower(maxXsize,maxYsize,region,thisrow,thiscol,
                                       orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));
              if((uint)thisrow==ecal_mc_cluster.SeedRow() && (uint)thiscol==ecal_mc_cluster.SeedCol()){
                ecal_mc_cluster.SetSeedEnergy(orig_energy*(thisfrac1*0.90 + thisfrac2*0.05 + thisfrac3*0.04));                
              }
              if ( msgLevel(MSG::DEBUG) ) debug()<<"\t"<< "ListLinePlot[{{"
                                                 <<min_x<<","<<min_y<<"},{"
                                                 <<min_x<<","<<max_y<<"},{"
                                                 <<max_x<<","<<max_y<<"},{"
                                                 <<max_x<<","<<min_y<<"},{"
                                                 <<min_x<<","<<min_y<<"}}],"<<endmsg;
              
            }
          }//end multiRegion checks
        }//loop on j = ybins
      }//loop on i = xbins
      ecal_tower_pos_and_energy.push_back(ecal_mc_cluster);
    }//neutrals
       
  }//mc loop
  
  //spill over for ecal
  //find all cells which have the same index, row, col with different seeds.
  //HERE
  if ( msgLevel(MSG::DEBUG) ) debug()<<"now looking for overlaps in "<<ecal_tower_pos_and_energy.size()<<" clusters"<<endmsg;
  if(ecal_tower_pos_and_energy.size()>1){    
    std::map<std::tuple<std::string,int,int>, int> firedCells;

    for(auto ecalCluster1 : ecal_tower_pos_and_energy){//loop over MC parts, effectively      
      for( auto clusterCell : ecalCluster1.Towers()){
        std::tuple<std::string,int,int>thistup = std::make_tuple(clusterCell.Region(),clusterCell.Row(),clusterCell.Col());        
        if(firedCells.find(thistup)!=firedCells.end()){//cell exists.
          firedCells[thistup]+=1;
        }
        else{firedCells[thistup]=1;}
      }//end loop on cells
    }//end loop on clusters
    if ( msgLevel(MSG::DEBUG) ) debug()<<"found "<<firedCells.size()<<" total cells"<<endmsg;
    for(auto contr_cell : firedCells){
      if(contr_cell.second>=1){
        if ( msgLevel(MSG::DEBUG) ) debug()<<"cell in region "<<std::get<0>(contr_cell.first)<<", (row, col) = ("
              <<std::get<1>(contr_cell.first) <<","<< std::get<2>(contr_cell.first)<<") has spill over"<<endmsg;
        //sum energy
        float ecell = 0;
        for(auto clus : ecal_tower_pos_and_energy){
          for(auto cel : clus.Towers()){
            if(cel.Region()==std::get<0>(contr_cell.first) 
               && cel.Row()==std::get<1>(contr_cell.first) 
               && cel.Col()==std::get<2>(contr_cell.first)){
              if ( msgLevel(MSG::DEBUG) ) debug()<<"adding energy "<<cel.E()<<"to total energy of cell ("
                    <<cel.Region()<<","<<cel.Row()<<", "<<cel.Col()<<")"<<endmsg;              
              ecell+=cel.E();
            }
            
          }//cells
        }//clusters
        //reset energy
        for(auto clus : ecal_tower_pos_and_energy){
          for(auto cel : clus.Towers()){
            if(cel.Region()==std::get<0>(contr_cell.first) 
               && cel.Row()==std::get<1>(contr_cell.first) 
               && cel.Col()==std::get<2>(contr_cell.first)){
              if ( msgLevel(MSG::DEBUG) ) debug()<<"resetting cell "<<cel.Region()<<","<<cel.Row()<<", "<<cel.Col()<<"with energy "<<ecell<<endmsg;
              
              cel.SetE(ecell);            
            }//end if
          }//end of second cell loop.
        }//end of second cluster loop
        
      }//end of multiple fired cells            
    }//end of fired cells    
  }//end size>1 cluster

  //make reconstructed clusters
  //this gets a bit messy. First, make all the CaloCellIDs, associate them to the digits.
  //then put the digits in the TES. Due to the CaloClusterEntry relying on SmartRefs, loop over
  //the digits in the TES and match them to our own list.
  //from there, make the CaloCluster, and then put it in the TES.
  //The Protoparticle is put in first, reliant on the hypo, which is reliant on the digit.
  //remoivng the smart ref reliance on the ClusterEntry would reduce the complexity.
  std::vector<LHCb::CaloDigit*> digit_cont;
  
  for(auto ecalCluster: ecal_tower_pos_and_energy){
    //std::vector<LHCb::CaloDigit* > digit_cont;    
    LHCb::ProtoParticle *proto = new LHCb::ProtoParticle;
    //cID.setCenterSize(center,the_area);
    //cID.setValid(true);
    //build digits
    
    for(auto ecalCell : ecalCluster.Towers()){      
      LHCb::CaloCellID cID("Ecal",ecalCell.Region(),ecalCell.Row(),ecalCell.Col());//region,row,column      
      LHCb::CaloDigit* cDig = new LHCb::CaloDigit(cID,ecalCell.E());
      //check if the digit is already inserted into the TES
      bool manual_check = false;
      for(auto thing : digit_cont){
        if(cID.hash() == thing->cellID().hash()){
          manual_check = true;
          if ( msgLevel(MSG::DEBUG) ) debug()<<"digit is already in TES!"<<endmsg;
          
        }
      }
      
      
      if ( msgLevel(MSG::DEBUG) ) debug()
        <<"check if the calo digit cell thing is in the container gives: "<<manual_check
        <<endmsg;
      
      if( //std::find_if(digits_and_cellids.begin(), digits_and_cellids.end(),
         //              [&toFind](const std::pair<LHCb::CaloDigit*, LHCb::CaloCellID*> x) {
         //                return x.first->e() == toFind.first->e() && x.second->hash()==toFind.second->hash();}
         //              )!=digits_and_cellids.end()
         manual_check
         ){//FOUND
        if ( msgLevel(MSG::DEBUG) ) debug()<<"already have digit and cell here"<<endmsg;
        continue;        
      }
      if ( msgLevel(MSG::DEBUG) ) debug()<<"inserting calo digit"<<cDig<<endmsg;
      if ( msgLevel(MSG::DEBUG) ) debug()<<"hash of calo cellID is"<<cDig->cellID().hash()<<", row "<<cDig->cellID().row()<<
        ", col"<<cDig->cellID().col()<<", area = "<<cDig->cellID().area()<<endmsg;
      
      caloDigits->insert(cDig);
      //if ( msgLevel(MSG::DEBUG) ) debug()<<"inserted calo digit"<<endmsg;
           
      digit_cont.push_back(cDig);
      //m_cellIDs.push_back(cID);
      //m_digits.push_back(cDig);
    }
        
    //make covariance
      
    double reco_energy = 0;
    double barycenter_x = 0;
    double barycenter_y = 0;
    double cov_xx=0;
    double cov_yy=0;
    double cov_ee=0;
    double cov_ex=0;
    double cov_ey=0;
    double cov_xy=0;
    double mean_x=0;
    double mean_y=0;
    
    for(auto clustered_cell :ecalCluster.Towers()){
      //int thisBorderIndex = clustered_cell.Region();
      int thisrow = clustered_cell.Row();
      int thiscol =clustered_cell.Col();
      float thisEfrac =clustered_cell.E();
      reco_energy+=thisEfrac;      
      auto min_x = (thiscol -m_cellCenterX)*clustered_cell.GetCellXsize();
      auto max_x = (thiscol+1 -m_cellCenterX)*clustered_cell.GetCellXsize();
      auto min_y = (thisrow-m_cellCenterY)*clustered_cell.GetCellYsize();
      auto max_y = (thisrow+1 - m_cellCenterY)*clustered_cell.GetCellYsize();
      auto xclus = (max_x-min_x)/2.+min_x;
      auto yclus = (max_y-min_y)/2.+min_y;
      mean_x+=xclus;
      mean_y+=yclus;
      
      barycenter_x+=thisEfrac*xclus;
      barycenter_y+=thisEfrac*yclus;

    }
    
    mean_x/=ecal_tower_pos_and_energy.size();
    mean_y/=ecal_tower_pos_and_energy.size();      
    //covariance matrix
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Calculating covariance"<<endmsg;
    for(auto cell_i: ecalCluster.Towers()){
      //int thisBorderIndexi = cell_i.Region();
      int thisrowi = cell_i.Row();
      int thiscoli = cell_i.Col();      
      float thisEfraci = cell_i.E();      
      
      auto min_xi = (thiscoli -m_cellCenterX)*cell_i.GetCellXsize();
      auto max_xi = (thiscoli+1 -m_cellCenterX)*cell_i.GetCellXsize();
      auto min_yi = (thisrowi-m_cellCenterY)*cell_i.GetCellYsize();
      auto max_yi = (thisrowi+1 - m_cellCenterY)*cell_i.GetCellYsize();
      auto xclusi = (max_xi-min_xi)/2.+min_xi;
      auto yclusi = (max_yi-min_yi)/2.+min_yi;
        
      cov_xx+=(xclusi-mean_x)*(xclusi-mean_x);
      cov_yy+=(yclusi-mean_y)*(yclusi-mean_y);
      cov_ee+=(thisEfraci - reco_energy/ecalCluster.Towers().size())*(thisEfraci - reco_energy/ecalCluster.Towers().size());
      cov_ex+=(thisEfraci - reco_energy/ecalCluster.Towers().size())*(xclusi-mean_x);
      cov_ey+=(thisEfraci - reco_energy/ecalCluster.Towers().size())*(yclusi-mean_y);
      cov_xy+=(xclusi-mean_x)*(yclusi-mean_y);
    }
    
    cov_xx/=ecal_tower_pos_and_energy.size();
    cov_yy/=ecal_tower_pos_and_energy.size();
    cov_ee/=ecal_tower_pos_and_energy.size();
    cov_ex/=ecal_tower_pos_and_energy.size();
    cov_ey/=ecal_tower_pos_and_energy.size();
    cov_xy/=ecal_tower_pos_and_energy.size();
    
    LHCb::CaloPosition *cPos = new LHCb::CaloPosition();
    
    if ( msgLevel(MSG::DEBUG) ) debug()<<"setting barycentre"<<endmsg;
    
    cPos->setParameters(Gaudi::Vector3(barycenter_x/reco_energy,barycenter_y/reco_energy,reco_energy));
    cPos->setZ(12000.);
    
    ///get the neighboring cells
    //bool multiRegion = false;
    
    
    //make covariance matrix based on the neighboring cells
    if ( msgLevel(MSG::DEBUG) ) debug()<<"setting covariance matrix"<<endmsg;
    
    Gaudi::SymMatrix3x3 thisCov;
    thisCov(LHCb::CaloPosition::X,LHCb::CaloPosition::X)=cov_xx;
    thisCov(LHCb::CaloPosition::Y,LHCb::CaloPosition::X)=cov_xy;
    thisCov(LHCb::CaloPosition::E,LHCb::CaloPosition::X)=cov_ex;
    thisCov(LHCb::CaloPosition::Y,LHCb::CaloPosition::Y)=cov_yy;
    thisCov(LHCb::CaloPosition::E,LHCb::CaloPosition::Y)=cov_ey;
    thisCov(LHCb::CaloPosition::E,LHCb::CaloPosition::E)=cov_ee;
    
    cPos->setCovariance(thisCov);
    //m_cpos.push_back(cPos);
    //insert cluster
    LHCb::CaloCluster* cluster = new LHCb::CaloCluster();
    LHCb::CaloDigitStatus::Status isSeedStatus = LHCb::CaloDigitStatus::SeedCell;
    LHCb::CaloDigitStatus::Status owned  = LHCb::CaloDigitStatus::OwnedCell ;
    for(SmartRef<LHCb::CaloDigit> toFind : digit_cont){
      //      if ( msgLevel(MSG::DEBUG) ) debug()<<"test of smart ref loop"<<endmsg;
      //if ( msgLevel(MSG::DEBUG) ) debug()<<"check of area: toFind: "<< CaloCellCode::caloArea(CaloCellCode::caloNum("Ecal"),toFind->cellID().area())<<", cluster "<<m_RegionNames.at(ecalCluster.SeedRegion())<<endmsg;
      if(CaloCellCode::caloArea(CaloCellCode::caloNum("Ecal"),toFind->cellID().area())==ecalCluster.SeedRegion()
         && toFind->cellID().row()==ecalCluster.SeedRow() 
         && toFind->cellID().col()==ecalCluster.SeedCol()){

        if ( msgLevel(MSG::DEBUG) ) debug()<<"got seed!"<<endmsg;
        cluster->entries().push_back( LHCb::CaloClusterEntry( toFind , isSeedStatus, 
                                                              ecalCluster.SeedEnergy()/ecalCluster.ClusterEnergy() ) );
        cluster->setSeed( toFind->cellID() );
        
      }
      //else, find out if the digit itself is in the cluster
      else{
        for(auto contrCell : ecalCluster.Towers()){
          if(toFind->cellID().row()==(uint)contrCell.Row() &&
             toFind->cellID().col()==(uint)contrCell.Col() &&
             CaloCellCode::caloArea(CaloCellCode::caloNum("Ecal"), toFind->cellID().area())==contrCell.Region()){
            if ( msgLevel(MSG::DEBUG) ) debug()<<"found contributing cell!"<<endmsg;
            cluster->entries().push_back(LHCb::CaloClusterEntry( toFind, owned,
                                                                 contrCell.E()/ecalCluster.ClusterEnergy()
                                                                 ));
          }//endif
        }//end loop on other cells
      }//end else
    }//end loop on smartrefs
    
    //cluster->setSeed(seedID );
    cluster->setPosition(*cPos);
    caloClusters->insert(cluster,ecalCluster.MCKey());
    
    
    LHCb::CaloHypo* hypo = new LHCb::CaloHypo(); //from digi
    //hypo->setPosition(cPos);
    hypo->setPosition(std::make_unique<LHCb::CaloPosition>(*cPos));
    
    if ( msgLevel(MSG::DEBUG) ) debug()<<"adding digits"<<endmsg;
    for(auto dig : digit_cont){      
      hypo->addToDigits(dig); 
    }    
    hypo->setHypothesis(LHCb::CaloHypo::Hypothesis::Photon);
    hypo->addToClusters(cluster);
    if ( msgLevel(MSG::DEBUG) ) debug()<<"inserting calo hypo"<<hypo<<endmsg;
    caloHypoContainer->insert(hypo,ecalCluster.MCKey());
    //else  hypo->setHypothesis(LHCb::CaloHypo::Hypothesis::NeutralHadron);//just for now.
    
    proto->addToCalo(hypo);
    auto pflag= LHCb::ProtoParticle::IsPhoton;
    //auto hflag = CaloDataType::isPhoton;  
    //const auto data =  m_estimator->data(&hypo, hflag , +1 );
    proto->addInfo(pflag,+1);

    //insert into TES
    if ( msgLevel(MSG::DEBUG) ) debug()<<"trying to print proto"<<endmsg;
    
    if ( msgLevel(MSG::DEBUG) ) debug()<<"proto = "<<*proto<<endmsg;
    //if ( msgLevel(MSG::DEBUG) ) debug()<<"part->key() = "<<part->key()<<endmsg;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"part->key() from cluster = "<< ecalCluster.MCKey()<<endmsg;
    if ( msgLevel(MSG::DEBUG) ) debug()<<"inserting proto"<<endmsg;
    
    neutralProtoContainer->insert(proto,ecalCluster.MCKey());
    //if ( msgLevel(MSG::DEBUG) ) 
    if ( msgLevel(MSG::DEBUG) ) debug()<<"inserted " << proto << endmsg;
    //m_protos.push_back(*proto);    
  }

  
    

  return StatusCode::SUCCESS;
}



//=============================================================================
//  Finalize
//=============================================================================
StatusCode DelphesProto::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  

  return GaudiAlgorithm::finalize();  // must be called after all other actions
}


std::vector<std::tuple<float,float> > DelphesProto::PointsOfIntersection(float xhit, float yhit,
                                                      float xmin, float xmax,
                                                      float ymin, float ymax, float rM){
  //compute all points of intersection of circle of radius rM with center (xhit,yhit)
  //and box (xmin,ymin), (xmax,ymax)
  std::vector<std::tuple<float,float>> ret;  
  //first, x==xmin points
  if(rM >=fabs(xhit-xmin)){//make sure sqrt is defined
    float thing = sqrt(rM*rM - (xhit-xmin)*(xhit-xmin));    
    float yI1= yhit + thing;
    float yI2 = yhit- thing;
    if (yI1<=ymax && yI1>=ymin){ret.push_back(std::make_tuple(xmin,yI1));}
    if (yI2<=ymax && yI2>=ymin){ret.push_back(std::make_tuple(xmin,yI2));}    
  }
  //x==xmax
  if(rM>=fabs(xmax-xhit)){
    float thing = sqrt(rM*rM - (xmax-xhit)*(xmax-xhit));    
    float yI1= yhit + thing;
    float yI2 = yhit- thing;
    if (yI1<=ymax && yI1>=ymin){ret.push_back(std::make_tuple(xmax,yI1));}
    if (yI2<=ymax && yI2>=ymin){ret.push_back(std::make_tuple(xmax,yI2));}    
  }
  //y==ymin
  if(rM >=fabs(yhit-ymin)){
    float thing = sqrt(rM*rM - (yhit-ymin)*(yhit-ymin));    
    float xI1= xhit + thing;
    float xI2 = xhit- thing;
    if (xI1<=xmax && xI1>=xmin){ret.push_back(std::make_tuple(xI1,ymin));}
    if (xI2<=xmax && xI2>=xmin){ret.push_back(std::make_tuple(xI2,ymin));}    
  }
  //y==ymax
  if(rM>=fabs(ymax-yhit)){
    float thing = sqrt(rM*rM - (ymax-yhit)*(ymax-yhit));    
    float xI1= xhit + thing;
    float xI2 = xhit- thing;
    if (xI1<=xmax && xI1>=xmin){ret.push_back(std::make_tuple(xI1,ymax));}
    if (xI2<=xmax && xI2>=xmin){ret.push_back(std::make_tuple(xI2,ymax));}    
  }
  return ret;
  
}

float DelphesProto::EnergyFraction(float x1, float x2, float y1, float y2, float num_RM, int num_integration_points){
  //numerical integration of region bounded by cell (x1,y1),(x2,y2) and n moliere radii
  //do they intersect?
  //circle is x^2 + y^2 = R^2
  //find points where circle crosses.
  //todo, update to ellipse. for angles
  std::vector<std::tuple< float, float> > points2check  = PointsOfIntersection(0,0,x1,x2,y1,y2,num_RM*m_moliere_radius);
  if(points2check.size()==0 
     && x1*x1+y1*y1>=num_RM*m_moliere_radius*num_RM*m_moliere_radius 
     && x2*x2+y2*y2>=num_RM*m_moliere_radius*num_RM*m_moliere_radius){//no points of intersection
    //either there are no points that intersect the circle,
    //or the cell is entirely in the circle
    //if ( msgLevel(MSG::DEBUG) ) debug()<<"no points. continuing"<<endmsg;
  return 0.;}
  
  //for(auto pt : points2check){
    //if ( msgLevel(MSG::DEBUG) ) debug()<<"got point ("<<std::get<0>(pt)<<", "<<std::get<1>(pt)<<")"<<endmsg;
  //}
  int this_int = 0;
  for(int i=0; i<num_integration_points; ++i){    
    //use MC integration to get the area inside this b
    //first throw random point into the circle
    auto xpos = m_flatDist() * (x2-x1) + x1 ;//m_Rand.Uniform(x1,x2);
    auto ypos = m_flatDist() * (y2-y1) + y1;//m_Rand.Uniform(y1,y2);
    //now if the point is inside the region we want, then iterate
    if(xpos*xpos+ypos*ypos<(num_RM*m_moliere_radius)*(num_RM*m_moliere_radius)){//in the circle.
      this_int+=1;
    }
  }
  //if ( msgLevel(MSG::DEBUG) ) debug()<<"integral = "<<this_int<<", fraction = "<<(double)this_int/num_integration_points.<<endmsg;
  //energy fraction is fraction of total circle in this square,
  //so the area of the circle in the square is the MC integral * area of square
  //fraction of circle is area of intesection / pi r^2
  //if ( msgLevel(MSG::DEBUG) ) debug()<<"area of square = "<<(x2-x1)<<"*"<<(y2-y1)<<endmsg;
  //if ( msgLevel(MSG::DEBUG) ) debug()<<"area of circle = "<<3.14159*num_RM*num_RM*m_moliere_radius*m_moliere_radius<<endmsg;
  
  double ret = (double)this_int/((double)num_integration_points) *(x2-x1)*(y2-y1)/(3.14159*num_RM*m_moliere_radius*num_RM*m_moliere_radius);
  
  return ret;
}

  
//=============================================================================
//  Additional Infos
//=============================================================================
std::vector<double> DelphesProto::AddInfo(double oneOverP, std::vector<std::vector<double>> lutMatrix) {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Adding Info" << endmsg;
  
  std::vector<double> addedInfo;
  
  for(uint rows = 0; rows<lutMatrix.size(); rows++)
  {
    if((oneOverP >= lutMatrix[rows][0]) && (oneOverP <= lutMatrix[rows][1]))
    {
      // cout<<"size: "<<lutMatrix[rows].size()<<endl; 
      for(uint iInfo=2; iInfo<lutMatrix[rows].size(); iInfo++)
        addedInfo.push_back(lutMatrix[rows][iInfo]);      
      break;      
    }    
  }
  return addedInfo;  
}
