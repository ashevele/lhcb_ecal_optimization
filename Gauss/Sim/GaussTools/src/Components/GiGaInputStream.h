#ifndef GIGA_GIGAINPUTSTREAM_H
#define GIGA_GIGAINPUTSTREAM_H 1

// include
// local (GiGa)
#include "GiGaStream.h"
// forward decclaartion
//template <class ALGORITHM> class AlgFactory ;

/** @class GiGaInputStream GiGaInputStream.h component/GiGaInputStream.h
 *
 *  Input stream for GiGa
 *
 *  @author Ivan Belyaev
 *  @date   15/01/2002
 */

class GiGaInputStream : virtual public GiGaStream
{
public:

  StatusCode execute   () override;    ///< Algorithm execution

  /** Standard constructor
   *  @param name stream(algorithm) name
   *  @param pSvcLoc pointer to the Service Locator
   */
  GiGaInputStream( const std::string& name     ,
                   ISvcLocator*       pSvcLoc  ) ;

  /** destructor
   */
  virtual ~GiGaInputStream();

};

// ============================================================================
// The End
// ============================================================================
#endif // GIGA_GIGAINPUTSTREAM_H
// ============================================================================
