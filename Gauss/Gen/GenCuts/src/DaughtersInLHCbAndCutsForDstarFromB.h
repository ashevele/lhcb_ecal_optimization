#ifndef GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTARFROMB_H
#define GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTARFROMB_H 1

// Include files
#include "DaughtersInLHCbAndCutsForDstar.h"

/** @class DaughtersInLHCbAndCutsForDstarFromB DaughtersInLHCbAndCutsForDstarFromB.h
 *
 *  Tool to select D* particles from a b-hadron with pT cuts
 *
 *  @author Adam Morris
 *  @date   2018-03-30
 */
class DaughtersInLHCbAndCutsForDstarFromB : public DaughtersInLHCbAndCutsForDstar, virtual public IGenCutTool {
public:
  /// Standard constructor
  DaughtersInLHCbAndCutsForDstarFromB( const std::string & type,
                                     const std::string & name,
                                     const IInterface * parent ) ;

  virtual ~DaughtersInLHCbAndCutsForDstarFromB() ; ///< Destructor

  /// Initialization
  virtual StatusCode initialize();

  /** Check that the signal D* satisfies the cuts in
   *  SignalIsFromBDecay and DaughtersInLHCbAndCutsForDstar.
   *  Implements IGenCutTool::applyCut.
   */
  virtual bool applyCut( ParticleVector & theParticleVector,
                         const HepMC::GenEvent * theEvent,
                         const LHCb::GenCollision * theCollision ) const ;

private:
  /// From a b cut tool
  const IGenCutTool * m_fromBcuts;
} ;
#endif // GENCUTS_DAUGHTERSINLHCBANDCUTSFORDSTARFROMB_H
