// Gaudi.
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/ParticleProperty.h"

// Generators.
#include "Generators/IBeamTool.h"

// LbSuperChic2.
#include "LHAupSuperChic2.h"
#include "SuperChic2Production.h"

//-----------------------------------------------------------------------------
//  Implementation file for class: SuperChic2Production
//
//  2016-08-30 : Philip Ilten
//-----------------------------------------------------------------------------

// Declaration of the tool factory.
DECLARE_TOOL_FACTORY(SuperChic2Production)

//=============================================================================
// Default constructor.
//=============================================================================
SuperChic2Production::SuperChic2Production(const std::string &type,
					   const std::string &name,
					   const IInterface *parent)
  : HardProduction (type, name, parent) {
  // Declare the tool properties.
  declareInterface<IProductionTool>(this);
}

//=============================================================================
// Initialize the hard process tool.
//=============================================================================
StatusCode SuperChic2Production::hardInitialize() {

  // Retrieve the Pythia8 production tool.
  if (!m_pythia8) m_pythia8 = dynamic_cast<Pythia8Production*>
		    (tool<IProductionTool>("Pythia8Production", this));
  if (!m_pythia8) return Error("Failed to retrieve Pythia8Production tool.");
  m_hard = m_pythia8;
  m_pythia8->m_beamToolName = m_beamToolName;

  // Initialize the beam tool.
  if (!m_beamTool) m_beamTool = tool<IBeamTool>(m_beamToolName, this);
  if (!m_beamTool) return Error("Failed to initialize the IBeamTool.");

  // Set Pythia 8 LHAup and UserHooks (no UserHooks needed).
  m_lhaup = new Pythia8::LHAupSuperChic2(m_pythia8->m_pythia);

  // Read default settings.
  for (unsigned int setting = 0; setting < m_defaultSettings.size();
       ++setting) {
    debug() << m_defaultSettings[setting] << endmsg;
    m_pythia8->m_pythia->readString(m_defaultSettings[setting]);
  }

  // Read user settings.
  for (unsigned int setting = 0; setting < m_userSettings.size(); ++setting) {
    debug() << m_userSettings[setting] << endmsg;
    m_pythia8->m_pythia->readString(m_userSettings[setting]);
  }

  // Set the COM.
  Gaudi::XYZVector pBeam1, pBeam2;
  m_beamTool->getMeanBeams(pBeam1, pBeam2);
  double e1(sqrt(pBeam1.Mag2())), e2(sqrt(pBeam2.Mag2()));
  double ecm(sqrt((e1 + e2)*(e1 + e2) -
		  (pBeam1 + pBeam2).Mag2())/Gaudi::Units::GeV);
  m_pythia8->m_pythia->settings.parm("SuperChic2:rts", ecm);
  m_pythia8->m_pythia->settings.parm("SuperChic2:mmax", ecm);
  return StatusCode::SUCCESS;
}

//=============================================================================
// The END.
//=============================================================================
