#ifndef LBSUPERCHIC2_LHAUPSUPERCHIC2_H
#define LBSUPERCHIC2_LHAUPSUPERCHIC2_H

// System.
#include <sys/stat.h>
#include <unistd.h>

// Pythia 8.
#include "Pythia8/Pythia.h"
#include "Pythia8/LesHouches.h"

/**
 * Pythia 8 Les Houches Accord (LHA) user process tool for SuperChic2.
 * 
 * All SuperChic settings are passed via the Pythia setting scheme and
 * are given the name "SuperChic:<setting>" where "<setting>" is one
 * of the settings given in the SuperChic manual. The options "nev"
 * and "erec" are not provided, as neither of these are used (they set
 * the number of un-weighted events and the event record format,
 * respectively). The setting "SuperChic:decays" has also been
 * added. If on, SuperChic will be used to perform all the decays that
 * it knows how to perform. If off, then all unstable particles are
 * decayed by Pythia. Note that in the decays by Pythia, resonances
 * (such as the W or Higgs) are not treated as
 * resonances. Consequently, changes in branching fraction will not
 * change the cross-section.
 * 
 * @class  LHAupSuperChic2
 * @file   LHAupSuperChic2.h 
 * @author Philip Ilten
 * @date   2016-08-30
 */
namespace Pythia8 {
  class LHAupSuperChic2 : public LHAup {
    
  public:

    /**
     * Constructor. Creates the needed settings in Pythia.
     * 
     * Needed settings for frame type, MPI, and ISR are passed to
     * Pythia. All relevant SuperChic2 settings are added to the
     * Pythia 8 settings database. Additionally, the directory
     * structure needed for running is created.
     */
    LHAupSuperChic2(Pythia *pythiaIn, string dirIn = "superchicrun");
    
    /**
     * Initialize the SuperChic2 generator.
     *
     * All settings from the Pythia 8 settings database are passed to
     * the SuperChic2 generator. Masses and SM parameters are also
     * pulled from the database. If needed grid initialization is
     * performed for SuperChic2, otherwise this step is
     * skipped. SuperChic2 is then initialized and the process is
     * added to the Pythia 8 LHA processes.
     */
    virtual bool setInit();
    
    /**
     * Generate an event with SuperChic2.
     *
     * First, unweighted events are generated with SuperChic2 if
     * needed. Next, the particles for an event are transferred to the
     * LHEF event record and read into Pythia 8. Finally, the
     * cross-section and PDF information is set.
     */
    virtual bool setEvent(int idProcIn = 0);
    
  private:
    
    /// Copy a string to a FORTRAN variable.
    void chrcpy(char *f, string c, int n);
    
    /// The PYTHIA object.
    Pythia *pythia; 
    
    /// The run directory and settings prefix.
    string dir, pre;
    
    /// Flag to run Pythia decays.
    bool decays;
    
    /// The current working directory.
    char cwd[FILENAME_MAX];
  };
}

#endif // LBSUPERCHIC2_LHAUPSUPERCHIC2_H
