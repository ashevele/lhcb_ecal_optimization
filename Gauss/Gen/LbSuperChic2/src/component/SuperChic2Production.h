#ifndef LBSUPERCHIC2_SUPERCHIC2PRODUCTION_H 
#define LBSUPERCHIC2_SUPERCHIC2PRODUCTION_H 1

// LbHard.
#include "LbHard/HardProduction.h"

/** 
 * Production tool to generate events with SuperChic2.
 * 
 * SuperChic2 is a dedicated Monte Carlo generator for the production
 * of central exclusive production (CEP) events. The SuperChic2 page
 * can be found at https://superchic.hepforge.org/ with documentation
 * provided in arXiv:1508.02718 and technical documentation in the
 * user manual https://superchic.hepforge.org/superchic2.pdf. Note
 * that SuperChic2 is a major version change from SuperChic, and
 * consequently both major version has its own production tool
 *
 * All processes produced by SuperChic2 are of the form p(1) p(2) ->
 * p(3) p(4) X(...), where p(1) and p(2) are the incoming protons,
 * p(3) and p(4) are the outgoing protons, and X(...) are the
 * remaining particles in the event. Because some processes produce
 * final states with bare partons, this production tool is implemented
 * as a HardProduction tool with the showering nominally handled by
 * Pythia 8. Currently no other showering tools have been
 * interfaced. Note that neither MPI nor ISR are produced as these are
 * CEP events; only FSR is performed. The commands are handled through
 * the Pythia 8 settings interface and take the form
 * "SuperChic2:<setting> = <value>" where the options for <setting>
 * are given below with the type given in brackets. Settings marked as
 * ignored can be set, but either have no effect or are overwritten by
 * the tool.
 *
 *\n rts:       (parm) ignored, center of mass energy.
 *\n isurv:     (mode) soft survival mode (1 to 4).
 *\n intag:     (word) the input prefix, this does not need to be changed.
 *\n PDFname:   (word) LHAPDF PDF set name.
 *\n PDFmember: (mode) LHPAPDF PDF member.
 *\n proc:      (mode) process number enumerated in more detail below.
 *\n outtag:    (word) the output prefix, this does not need to be changed.
 *\n sfaci:     (flag) if "true" soft survival is run.
 *\n decays:    (flag) if "true" SuperChic2 performs the decays.
 *\n
 *\n ncall:     (mode) number of VEGAS calls for preconditioning.
 *\n itmx:      (mode) number of VEGAS iterations for preconditioning.
 *\n prec:      (parm) relative accuracy in main run.
 *\n ncall1:    (mode) number of calls in first iteration.
 *\n inccall:   (mode) number of calls to increase per iteration.
 *\n itend:     (mode) maximum number of iterations.
 *\n iseed:     (mode) ignored, random seed.
 *\n s2int:     (mode) survival factor for integration.
 *\n 
 *\n genunw:    (flag) ignored, if "true" generates unweighted events.
 *\n readwt:    (flag) if "true" use pre-calculated maximum weight.
 *\n wtmax:     (parm) the pre-calculated maximum weight to use.
 *\n ymin:      (parm) central system minimum rapidity.
 *\n ymax:      (parm) central system maximum rapidity.
 *\n mmin:      (parm) central system minimum mass.
 *\n mmax:      (parm) central system maximum mass.
 *\n gencuts:   (flag) if "true" apply additional kinematic cuts below.
 *\n scorr:     (flag) if "true" perform correlated decays.
 *\n fwidth:    (flag) if "true" use finite width for chi_c production.
 *\n
 *\n ptamin:    (parm) minimum pT (GeV) for p(5).
 *\n ptbmin:    (parm) minimum pT (GeV) for p(6).
 *\n ptcmin:    (parm) minimum pT (GeV) for p(7).
 *\n ptdmin:    (parm) minimum pT (GeV) for p(8).
 *\n ptemin:    (parm) minimum pT (GeV) for p(9).
 *\n ptfmin:    (parm) minimum pT (GeV) for p(10).
 *\n etaamin:   (parm) minimum eta for p(5). 
 *\n etabmin:   (parm) minimum eta for p(6). 
 *\n etacmin:   (parm) minimum eta for p(7). 
 *\n etadmin:   (parm) minimum eta for p(8). 
 *\n etaemin:   (parm) minimum eta for p(9). 
 *\n etafmin:   (parm) minimum eta for p(10).
 *\n etaamax:   (parm) maximum eta for p(5). 
 *\n etabmax:   (parm) maximum eta for p(6). 
 *\n etacmax:   (parm) maximum eta for p(7). 
 *\n etadmax:   (parm) maximum eta for p(8). 
 *\n etaemax:   (parm) maximum eta for p(9). 
 *\n etafmax:   (parm) maximum eta for p(10).
 *\n 
 *\n rjet:      (parm) R parameter for jet reconstruction.
 *\n jalg:      (word) jet algorithm ("antikt", "kt", or "Durham").
 *\n m2b:       (parm) ignored, p(4) and p(5) mass (GeV).
 *\n pdgid1:    (mode) p(4) PDG ID.
 *\n pdgid2:    (mode) p(5) PDG ID.
 *\n
 *\n The processes are as enumerated below.
 *\n 1:  Higgs --> bbar
 *\n 2:  diphoton CEP
 *\n 3:  gg dijets
 *\n 4:  qq dijets (massless)
 *\n 5:  cc dijets
 *\n 6:  bb dijets
 *\n 7:  ggg trijets
 *\n 8:  gqq trijets (massless)
 *\n 9:  pip pim
 *\n 10: pi0 pi0
 *\n 11: Kp Km
 *\n 12: K_0 K_0
 *\n 13: rho_0 rho_0
 *\n 14: eta eta
 *\n 15: eta eta'
 *\n 16: eta' eta'
 *\n 17: phi phi
 *\n 18: J/psi J/psi (muonic decays)
 *\n 19: J/psi psi(2S) (muonic decays)
 *\n 20: psi(2S) psi(2S) (muonic decays)
 *\n 21: chi_c0 -> J/psi+gamma -> mu+mu- + gamma
 *\n 22: chi_c1 -> J/psi+gamma -> mu+mu- + gamma
 *\n 23: chi_c2 -> J/psi+gamma -> mu+mu- + gamma
 *\n 24: chi_c0 -> 2 body
 *\n 25: chi_c1 -> 2 body (scalar)
 *\n 26: chi_c2 -> 2 body (scalar)
 *\n 27: chi_c1 -> 2 body (fermion)
 *\n 28: chi_c2 -> 2 body (fermion)
 *\n 29: chi_c0 -> 2(pi+pi-)
 *\n 30: chi_c1 -> 2(pi+pi-)
 *\n 31: chi_c2 -> 2(pi+pi-)
 *\n 32: chi_c0 -> pi+pi- + K+K-
 *\n 33: chi_c1 -> pi+pi- + K+K-
 *\n 34: chi_c2 -> pi+pi- + K+K-
 *\n 35: chi_c0 -> 3(pi+pi-)
 *\n 36: chi_c1 -> 3(pi+pi-)
 *\n 37: chi_c2 -> 3(pi+pi-)
 *\n 38: eta_c
 *\n 39: chi_b0 -> Upsilon+gamma -> mu+mu- + gamma
 *\n 40: chi_b1 -> Upsilon+gamma -> mu+mu- + gamma
 *\n 41: chi_b2 -> Upsilon+gamma -> mu+mu- + gamma
 *\n 42: chi_b0 -> 2 body
 *\n 43: chi_b1 -> 2 body (scalar)
 *\n 44: chi_b2 -> 2 body (scalar)
 *\n 45: chi_b1 -> 2 body (fermion)
 *\n 46: chi_b2 -> 2 body (fermion)
 *\n 47: eta_b
 *\n 48: rho -> pi+pi- photoproduction
 *\n 49: phi -> K+K- photoproduction
 *\n 50: J/psi -> mu+mu- photoproduction
 *\n 51: Upsilon(1S) -> mu+mu- photoproduction
 *\n 52: Psi(2S) -> mu+mu- photoproduction
 *\n 53: Psi(2S) -> J/psi pi pi -> mu+mu- pi+pi- photoproduction
 *\n 54: gamma gamma -> W+W- -> mu+nu mu-nub
 *\n 55: gamma gamma -> W+W- -> e+nu e-nub 
 *\n 56: gamma gamma -> e+e- production
 *\n 57: gamma gamma -> mu+mu- production
 *\n 58: gamma gamma -> tau+tau- production
 *\n 59: gamma gamma -> gamma gamma production
 *\n 60: gamma gamma -> Higgs -> bbar production
 *\n 61: gamma gamma -> W+W- -> mu+nu mu-nub (electron)
 *\n 62: gamma gamma -> W+W- -> e+nu e-nub (electron)
 *\n 63: gamma gamma -> e+e- production (electron)
 *\n 64: gamma gamma -> mu+mu- production (electron)
 *\n 65: gamma gamma -> tau+tau- production (electron)
 *\n 66: gamma gamma -> gamma gamma production (electron)
 *\n 67: gamma gamma -> Higgs -> bbar production (electron)
 *
 * @class  SuperChic2Production
 * @file   SuperChic2Production.h 
 * @author Philip Ilten
 * @date   2016-08-30
 */
class SuperChic2Production : public HardProduction {
public:

  /// Default constructor.
  SuperChic2Production(const std::string &type, const std::string &name,
		       const IInterface *parent);

  /// Initialize the hard process tool.
  StatusCode hardInitialize();

private:

  // Members.
  CommandVector m_defaultSettings; ///< The default settings.

};

#endif // LBSUPERCHIC2_SUPERCHIC2PRODUCTION_H
