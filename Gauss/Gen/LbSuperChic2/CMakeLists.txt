################################################################################# Package: LbSuperChic2
################################################################################
gaudi_subdir(LbSuperChic2 v1r0)

gaudi_depends_on_subdirs(Gen/SuperChic2
                         Gen/LbHard)

gaudi_add_module(LbSuperChic2
                 src/component/*.cpp
                 LINK_LIBRARIES SuperChic2 LbHardLib)

