#ifndef MINTLINESHAPES_SINGLE_HAT_SHAPE_HH
#define MINTLINESHAPES_SINGLE_HAT_SHAPE_HH

#include <complex>
#include <string>
//
#include <complex>
#include <string>
#include <iostream>

#include "Mint/ILineshape.h"
#include "Mint/AssociatedDecayTree.h"
#include "Mint/DalitzEventAccess.h"
#include "Mint/IDalitzEventAccess.h"

#include "Mint/DalitzCoordinate.h"
#include "Mint/IGenFct.h"

using namespace std;
using namespace MINT;

class singleTopHatShape : public DalitzEventAccess, virtual public ILineshape{
 private:
  mutable MINT::counted_ptr<IGenFct> _genFct;
  void makeGeneratingFunction() const;

 protected:

  const AssociatedDecayTree& _theDecay;
  double _min_sij, _max_sij;

  double min()const{return _min_sij;}
  double max()const{return _max_sij;}

  bool startOfDecayChain() const{return !(_theDecay.hasParent());}
  double mumsRecoMass2() const;

 public:
  singleTopHatShape( const AssociatedDecayTree& decay
		     , IDalitzEventAccess* events
		     , double mini, double maxi);
  singleTopHatShape(const singleTopHatShape& other);

  std::complex<double> getVal() override;
  virtual std::complex<double> getValue() const; // not required, but useful
  std::complex<double> getValAtResonance() override {return 1;}
  std::complex<double> getSmootherLargerVal() override {return getVal();}

  DalitzCoordinate getDalitzCoordinate(double nSigma=3) const override;
  void print(std::ostream& out = std::cout) const override;
  std::string name() const override;
  MINT::counted_ptr<IGenFct> generatingFunction() const override =0;


  virtual ~singleTopHatShape(){};
};
std::ostream& operator<<(std::ostream& out, const singleTopHatShape& amp);

#endif
//
