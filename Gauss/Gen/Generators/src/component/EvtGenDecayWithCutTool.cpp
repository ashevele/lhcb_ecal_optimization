// Header file
#include "EvtGenDecayWithCutTool.h"

// Include files
#include <fstream>
#include <iostream>

// from Gaudi
#include "GaudiKernel/DeclareFactoryEntries.h"

// from LHCb
#include "GenEvent/HepMCUtils.h"

// from HepMC
#include "HepMC/GenEvent.h"
#include "HepMC/GenParticle.h"
#include "HepMC/GenVertex.h"

#include "MCInterfaces/IGenCutTool.h"

//-----------------------------------------------------------------------------
// Implementation file for class : EvtGenDecayWithCutTool
//
// Dominik Muller
// 2018-3-12
//-----------------------------------------------------------------------------
// Declaration of the Tool Factory

DECLARE_TOOL_FACTORY( EvtGenDecayWithCutTool )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
EvtGenDecayWithCutTool::EvtGenDecayWithCutTool( const std::string& type, const std::string& name,
                                                const IInterface* parent )
    : EvtGenDecay( type, name, parent )
{
  // Declare IEvtGenDecay interface
  declareInterface<IDecayTool>( this );
  // Name of the cut tool to be used
  declareProperty( "CutTool", m_cutToolName = "" );
}

//=============================================================================
// Initialize method
//=============================================================================
StatusCode EvtGenDecayWithCutTool::initialize()
{
  StatusCode sc = EvtGenDecay::initialize();
  if ( sc.isFailure() ) return sc;

  if ( m_cutToolName != "" ) m_cutTool = tool<IGenCutTool>( m_cutToolName, this );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Generate a Decay tree from a particle theMother in the event theEvent
// Repeat until it passes the the cut;
//=============================================================================
StatusCode EvtGenDecayWithCutTool::generateSignalDecay( HepMC::GenParticle* theMother, bool& flip ) const
{
  int counter    = 0;
  bool found_one = false;
  while ( !found_one ) {
    HepMCUtils::RemoveDaughters( theMother );
    StatusCode sc = EvtGenDecay::generateSignalDecay( theMother, flip );
    if ( sc.isFailure() ) {
      return sc;
    }

    IGenCutTool::ParticleVector theParticleList;
    theParticleList.push_back( theMother );
    if ( m_cutTool ) {
      // Passing nullptr for lack of anything else here.
      // Just hope the cut tool is not using those
      found_one = m_cutTool->applyCut( theParticleList, nullptr, nullptr );
    } else {
      // Skip this if no cut tool was provided
      found_one = true;
    }
    counter += 1;
  }
  debug() << "Needed " << counter << " attempts to get one that passes the cut." << endmsg;
  return StatusCode::SUCCESS;
}
