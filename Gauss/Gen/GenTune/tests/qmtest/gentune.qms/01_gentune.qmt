<?xml version="1.0" ?>
<!DOCTYPE extension
  PUBLIC '-//QM/2.4.1/Extension//EN'
  'http://www.codesourcery.com/qm/dtds/2.4.1/-//qm/2.4.1/extension//en.dtd'>
<extension class="GaudiTest.GaudiExeTest" kind="test">
<argument name="program"><text>gaudirun.py</text></argument>
<argument name="args"><set>
<text>$GENTUNEROOT/options/example/MC_Generic_Test.py</text>
</set></argument>
<argument name="use_temp_dir"><enumeral>true</enumeral></argument>
<argument name="validator"><text>
# Histograms output by test are expected to have same binning and names as in reference
# TODO: Improve YODA based histogram comparison by checking distribution variation
import yoda
import os
while (True):
    refPath = os.path.normpath(os.path.expandvars('$GENTUNEROOT/tests/refs/LHCb_MC_refhisto.yoda'))
    if not os.path.exists(refPath) or not os.path.isfile(refPath):
        causes.append(os.path.expandvars('No reference file for target $CMTCONFIG'))
        break
    hstPath = os.path.join(os.getcwd(), 'LHCb_MC_test.yoda')
    if not os.path.exists(hstPath) or not os.path.isfile(hstPath):
        causes.append('Histogram file not found or not plain file')
        break
    rhd = yoda.readYODA(refPath)
    thd = yoda.readYODA(hstPath)
    if thd is None or len(thd) == 0:
        causes.append('Parsing of test output histograms has failed in YODA')
        break
    bHasPathPrefix = not rhd.keys()[0] in thd.keys()
    refHNames = bHasPathPrefix and [ x.name for x in rhd.values() ] or list(rhd.keys())
    diffMsgs = []
    for k in refHNames:
        (basePath, hName) = os.path.split(k)
        if bHasPathPrefix: # do not compare internal counters (e.g. event counter)
            if basePath.startswith('_'):
                continue
        else:
            if hName.startswith('_'):
                continue
        if bHasPathPrefix:
            rh = rhd[filter(lambda x: x.endswith(k), rhd.keys())[0]]
            thn = filter(lambda x: x.endswith(k), thd.keys())
            if len(thn) != 1:
                diffMsgs.append('Histogram \'%s\' not found in test output.' % (k))
                continue
            th = thd[thn[0]]
        else:
            rh = rhd[k]
            if not thd.has_key(k):
                diffMsgs.append('Histogram \'%s\' not found in test output.' % (k))
                continue
            th = thd[k]
        if rh.__class__.__name__ != th.__class__.__name__:
            diffMsgs.append('Distribution \'%s\' stored in different objects in test (%s) and reference (%s) output.' % (k, th.__class__.__name__, rh.__class__.__name__))
            continue
        clsName = rh.__class__.__name__
        if rh.dim != th.dim:
            diffMsgs.append('Histogram \'%s\' has different dimensions in test (%d) and reference (%d).' % (k, th.dim, rh.dim))
            continue
        if clsName.startswith('Scatter'):
            if rh.numPoints != th.numPoints:
                diffMsgs.append('%s plot \'%s\' has different numPoints in test (%d) and reference (%d).' % (clsName, k, th.numPoints, rh.numPoints))
                continue
        else:
            bCmpErr = False
            if rh.dim > 1:
                if rh.numBinsX != th.numBinsX:
                    diffMsgs.append('%s \'%s\' has different numBinsX in test (%d) and reference (%d).' % (clsName, k, th.numBinsX, rh.numBinsX))
                    bCmpErr = True
                if rh.numBinsY != th.numBinsY:
                    diffMsgs.append('%s \'%s\' has different numBinsY in test (%d) and reference (%d).' % (clsName, k, th.numBinsY, rh.numBinsY))
                    bCmpErr = True
            else:
                if rh.numBins != th.numBins:
                    diffMsgs.append('%s \'%s\' has different numBins in test (%d) and reference (%d).' % (clsName, k, th.numBins, rh.numBins))
                    bCmpErr = True
            if bCmpErr:
                continue
            if rh.numEntries(False) > 0 and th.numEntries(False) == 0:
                diffMsgs.append('%s \'%s\' is empty in test output.' % (clsName, k))
                if th.numEntries() > 0:
                    diffMsgs[-1] += ' Output overflow bins contain entries.'
                continue
        # add further statistical tests here!
    if len(diffMsgs) > 0:
        result['YODA diffs'] = result.Quote("\n".join(diffMsgs))
        causes.append('Test output differs from reference.')
    break
print('Test has finished!')
</text></argument>
</extension>
