################################################################################
# Package: EvtGenExtras
################################################################################
gaudi_subdir(EvtGenExtras v3r13)

gaudi_depends_on_subdirs(Gen/EvtGen
                         Gen/Mint)

find_package(GSL)

find_package(CLHEP)
find_package(ROOT)
include_directories(SYSTEM ${CLHEP_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(EvtGenExtras
                  src/*.cpp
                  NO_PUBLIC_HEADERS
                  INCLUDE_DIRS GSL
                  LINK_LIBRARIES GSL EvtGen Mint)

