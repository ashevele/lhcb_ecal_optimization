##
## A container where CernVM-FS is up and running
##
FROM hepsw/cvmfs-base-cc7
MAINTAINER Alexey Boldyrev <alexey.boldyrev@cern.ch>

USER root
ENV USER root
ENV HOME /root
ENV VO_LHCB_SW_DIR	/cvmfs/lhcb.cern.ch
ENV MYSITEROOT		/cvmfs/lhcb.cern.ch/lib

# CVMFS trick
## make sure FUSE can be enabled
RUN if [[ ! -e /dev/fuse ]]; then mknod -m 666 /dev/fuse c 10 229; fi

WORKDIR /root

RUN mkdir -p /cvmfs/lhcb.cern.ch && \
    echo "lhcb.cern.ch /cvmfs/lhcb.cern.ch cvmfs defaults 0 0" >> /etc/fstab

# ADD dot-bashrc              $HOME/.bashrc

## make the whole container seamlessly executable
ENTRYPOINT ["/usr/bin/cubied"]
CMD ["bash"]

RUN yum -y update && yum -y install \
        gcc \
        gcc-c++ \
        make \
        # ROOT dependencies:
        git \
        cmake \
        binutils \
        libX11-devel \
        libXpm-devel \
        libXft-devel \
        libXext-devel \
        # ROOT optional packages:
        gcc-gfortran \
        openssl-devel \
        # pcre-devel \
        mesa-libGL-devel \
        mesa-libGLU-devel \
        glew-devel \
        ftgl-devel \
        mysql-devel \
        fftw-devel \
        cfitsio-devel \
        graphviz-devel \
        avahi-compat-libdns_sd-devel \
        # libldap-dev \
        # gsl-static \
        # See https://sft.its.cern.ch/jira/browse/ROOT-9333
        openldap-devel \
        # gsl-devel \
        python-devel \
        libxml2-devel \
        bc \
        libpng12-devel \
        zeromq-devel \
        hdf5-devel \
	# Gauss (GiGaVisUI) dependencies:
        motif \
        motif-devel \
    && \
        yum clean all && \
        rm -rf /var/lib/apt/lists/* && \
        rm -rf /var/cache/yum

# Set ROOT environment
ENV ROOTSYS         "/opt/root"
ENV PATH            "$ROOTSYS/bin:$ROOTSYS/bin/bin:$PATH"
ENV LD_LIBRARY_PATH "$ROOTSYS/lib:$LD_LIBRARY_PATH"
ENV DYLD_LIBRARY_PATH "$ROOTSYS/lib:$DYLD_LIBRARY_PATH"
ENV PYTHONPATH      "$ROOTSYS/lib:PYTHONPATH"

ADD https://root.cern.ch/download/root_v6.14.04.Linux-centos7-x86_64-gcc4.8.tar.gz /var/tmp/root.tar.gz
RUN tar xzf /var/tmp/root.tar.gz -C /opt && rm -f /var/tmp/root.tar.gz

# get a non apt managed pip
RUN curl -s https://bootstrap.pypa.io/get-pip.py | python -
RUN pip install --upgrade pip

# Build pip deps
RUN pip install --no-cache-dir \
        scipy \
        root-numpy \
        rootpy \
	root_pandas \
        tqdm \
        ipython \
        notebook \
        matplotlib

RUN pip install --no-cache-dir \
        scikit-learn \
        pandas

ENV NB_USER nb_user
ENV NB_UID 1000
ENV HOME /home/${NB_USER}

RUN useradd -ms /bin/bash \
        -u ${NB_UID} \
        ${NB_USER}

# Make sure the contents of our repo are in ${HOME}
COPY . ${HOME}
USER root
RUN chown -R ${NB_UID} ${HOME}
RUN mkdir /home/${NB_USER}/cmtuser

WORKDIR /home/${NB_USER}

## EOF
